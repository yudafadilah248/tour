<?php

use App\Http\Controllers\AboutController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\BusController;
use App\Http\Controllers\CarsController;
use App\Http\Controllers\ConditionsController;
use App\Http\Controllers\GaleriController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KontakController;
use App\Http\Controllers\MicesController;
use App\Http\Controllers\MobilController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\TestimoniController;
use App\Http\Controllers\TourCategoriesController;
use App\Http\Controllers\WisataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('sewa-mobil', [CarsController::class, 'mobil'])->name('sewa-mobil');
Route::get('sewa-bus', [MobilController::class, 'bus'])->name('sewa-bus');

Route::get('paket-wisata', [WisataController::class, 'index'])->name('paket-wisata');
Route::get('wisata/{slug}', [WisataController::class, 'wisata'])->name('wisata');
Route::get('detail-wisata/{slug}', [WisataController::class, 'detail_wisata'])->name('detail-wisata');

Route::get('mice', [MicesController::class, 'mice'])->name('mice');
Route::get('mice-event', [MicesController::class, 'mice_event'])->name('mice-event');

Route::get('blog', [BlogController::class, 'index'])->name('blog');
Route::get('detail-blog/{slug}', [BlogController::class, 'detail_blog'])->name('blog.detail-blog');
// Route::get('detail-blog', [BlogController::class, 'detail_blog'])->name('detail-blog');
Route::get('galeri', [GaleriController::class, 'galeri'])->name('galeri');
Route::get('about', [AboutController::class, 'index'])->name('about');
Route::get('kontak', [KontakController::class, 'index'])->name('kontak');

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::get('register', [AuthController::class, 'register'])->name('register');
Route::post('proses_register', [AuthController::class, 'proses_register'])->name('proses_register');
Route::post('proses_login', [AuthController::class, 'proses_login'])->name('proses_login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/register/verify-email/{token}', [AuthController::class, 'verifyMail'])->name('verifyMail');
// Route::resource('admin/dashboard', DashboardController::class);

Route::group(['middleware' => ['auth']], function () {
    Route::prefix('admin/')->name('admin.')->middleware(['admin_role'])->group(function () {
        Route::get('create_tour/{id}', [TourCategoriesController::class, 'create_tour'])->name('tour_categori.create_tour');
        Route::resource('dashboard', DashboardController::class);
        Route::resource('user', UserController::class);
        Route::put('update-password/{userid}', [UserController::class, 'update_password'])->name('user.update-password');
        Route::resource('artikel', ArtikelController::class);
        Route::resource('galeri', GaleriController::class);
        Route::resource('condition', ConditionsController::class);
        Route::put('update_condition', [ConditionsController::class, 'update_condition'])->name('profil.update_condition');
        Route::resource('cars', CarsController::class);
        Route::resource('mobil', MobilController::class);
        Route::resource('profil', ProfilController::class);
        Route::put('update_profil', [ProfilController::class, 'update_profil'])->name('profil.update_profil');
        Route::resource('testimoni', TestimoniController::class);
        Route::resource('mice', MicesController::class);
        Route::put('update_mice', [MicesController::class, 'update_mice'])->name('mice.update_mice');
        Route::resource('tour_categori', TourCategoriesController::class);
        Route::post('store_tour/{id}', [TourCategoriesController::class, 'store_tour'])->name('tour_categori.store_tour');
        Route::get('edit_tour/{id}', [TourCategoriesController::class, 'edit_tour'])->name('tour_categori.edit_tour');
        Route::put('update_tour/{id}', [TourCategoriesController::class, 'update_tour'])->name('tour_categori.update_tour');
        Route::delete('destroy_tour/{id}', [TourCategoriesController::class, 'destroy_tour'])->name('tour_categori.destroy_tour');
        Route::get('destroy_price/{id}', [TourCategoriesController::class, 'destroy_price'])->name('tour_categori.destroy_price');
        Route::get('edit_price/{id}', [TourCategoriesController::class, 'edit_price'])->name('tour_categori.edit_price');
        Route::post('/update_price/{id}', [TourCategoriesController::class, 'update_price'])->name('tour_categori.update_price');
    });
});


Route::prefix('frontend/')->name('frontend.')->group(function () {
    Route::get('home', function () {
        return view('frontend.home');
    });
    Route::get('about', function () {
        return view('frontend.about');
    });
    Route::get('detail_blog', function () {
        return view('frontend.detail_blog');
    });
    Route::get('detail_wisata', function () {
        return view('frontend.detail_wisata');
    });
    Route::get('galeri', function () {
        return view('frontend.galeri');
    });
    Route::get('kontak', function () {
        return view('frontend.kontak');
    });
    Route::get('paket_wisata', function () {
        return view('frontend.paket_wisata');
    });
    Route::get('sewa_bus', function () {
        return view('frontend.sewa_bus');
    });
    Route::get('sewa_mobil', function () {
        return view('frontend.sewa_mobil');
    });
    Route::get('mice', function () {
        return view('frontend.mice');
    });
    Route::get('mice_event', function () {
        return view('frontend.mice_event');
    });
});
