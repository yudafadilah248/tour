<?php

namespace App\Http\Controllers;

use App\Models\user;

use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Hash;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        // dd($user->toArray());
        $data['page_title'] = 'Manajemen User';
        $data['users'] = $user;
        return view('admin.user.index', $data);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah user';
        $data['edit_password'] = true;
        return view('admin.user.form_user', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'name' => 'required',
            'no_hp' => 'required',
            'email' => 'required',
            'password' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }
        $token = Str::random(60);
        $object = array(
            'name' => $request->name,
            'email' => $request->email,
            'role' => 'admin',
            'no_hp' => $request->no_hp,
            'password' => Hash::make($request->password),
            'token' => $token,
        );


        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('user', $request->image);
            $object['image'] = $image;
            // dd($object['image']);
        }

        User::create($object);
        return redirect()->route('admin.user.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        $data['page_title'] = 'Edit User';
        $data['user'] = $user;
        $data['edit_mode'] = true;
        $data['edit_password'] = false;
        return view('admin.user.form_user', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name' => 'required',
            'email' => 'required',
            'no_hp' => 'required',
            'avatar' => 'file|mimes:jpg,png',
        );
        // dd($rules);
        $validator = Validator::make($request->all(), $rules, $messages = [
            'mimes' => 'The :attribute must be a file of type: :values.',
            'required' => 'The :attribute field is required.'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }
        $object = array(
            'name' => $request->name,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
        );

        $current = user::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('user', $request->image);
            $object['image'] = $image;
            // dd($object['avatar']);
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }

        $current->update($object);
        return redirect()->route('admin.user.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request, $id)
    {
        $rules = [
            'new_password' => 'required|min:8',
            'password_confirmation' => 'required|same:new_password',
        ];
        $validator = Validator::make($request->all(), $rules,  $messages = [
            'required' => 'The :attribute field is required.',
            'min' => 'the :attribute field is required 8 characters.',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = User::find($id);
        $data->password = Hash::make($request->new_password);
        $data->save();

        return redirect()->route('admin.user.index')->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    public function destroy($id)
    {
        //
        $user = user::where('id', $id)->firstOrFail();
        File::delete('./uploads/' . $user->image);
        $user->delete();
        return redirect()->route('admin.user.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
