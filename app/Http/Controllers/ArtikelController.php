<?php

namespace App\Http\Controllers;

use App\Models\artikel;

use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use \Cviebrock\EloquentSluggable\Services\SlugService;


class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $artikel = artikel::all();
        // dd($artikel->toArray());
        $data['page_title'] = 'Manajemen artikel';
        $data['artikels'] = $artikel;
        return view('admin.artikel.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah artikel';
        return view('admin.artikel.form_artikel', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreartikelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'judul' => 'required',
            'status' => 'required',
            'artikel' => 'required',
            'kutipan' => 'required',
            'tanggal' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'judul' => $request->judul,
            'slug' => Str::slug($request->judul, '-'),
            'status' => $request->status,
            'artikel' => $request->artikel,
            'kutipan' => $request->kutipan,
            'tanggal' => $request->tanggal,
        );

        // dd($object);


        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('artikel', $request->image);
            $object['image'] = $image;
            // dd($object['image']);
        }

        artikel::create($object);
        return redirect()->route('admin.artikel.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function show(artikel $artikel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $artikel = artikel::find($id);
        $data['page_title'] = 'Edit artikel';
        $data['artikel'] = $artikel;
        $data['edit_mode'] = true;
        return view('admin.artikel.form_artikel', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateartikelRequest  $request
     * @param  \App\Models\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'judul' => 'required',
            'status' => 'required',
            'artikel' => 'required',
            'kutipan' => 'required',
            'tanggal' => 'required',
            'image' => 'file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'judul' => $request->judul,
            'slug' => Str::slug($request->judul, '-'),
            'status' => $request->status,
            'artikel' => $request->artikel,
            'kutipan' => $request->kutipan,
            'tanggal' => $request->tanggal,
        );

        $current = artikel::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('artikel', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }

        $current->update($object);
        return redirect()->route('admin.artikel.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $artikel = artikel::where('id', $id)->firstOrFail();
        File::delete('./uploads/' . $artikel->image);
        $artikel->delete();
        return redirect()->route('admin.artikel.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
