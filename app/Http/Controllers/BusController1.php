<?php

namespace App\Http\Controllers;

use App\Models\Bus;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bus()
    {
        $data['page_title'] = 'Sewa Bus';
        // $data['galeri'] = $galeri;
        return view('home', $data);
        //
    }
    public function index()
    {
        //
        $bus = Bus::all();
        // dd($galeri->toArray());
        $data['page_title'] = 'Manajemen Bus';
        $data['bus'] = $bus;
        return view('admin.bus.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah Bus';
        return view('admin.bus.form_bus', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'harga' => 'required',
            'diskon' => 'required',
            'all_in' => 'required',
            'seat' => 'required',
            'type' => 'required',
            'transmisi' => 'required',
            'p3k' => 'required',
            'audio' => 'required',
            'ac' => 'required',
            'charger' => 'required',
            'bensin' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'harga' => $request->harga,
            'diskon' => $request->diskon,
            'all_in' => $request->all_in,
            'seat' => $request->seat,
            'type' => $request->type,
            'transmisi' => $request->transmisi,
            'p3k' => $request->p3k,
            'audio' => $request->audio,
            'ac' => $request->ac,
            'charger' => $request->charger,
            'bensin' => $request->bensin,
        );

        // dd($object);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('bus', $request->image);
            $object['image'] = $image;
            // dd($object['image']);
        }

        bus::create($object);
        return redirect()->route('admin.bues.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bus  $bus
     * @return \Illuminate\Http\Response
     */
    public function show(Bus $bus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bus  $bus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bus = Bus::find($id);
        // dd($galeri->toArray());
        $data['page_title'] = 'Edit Bus';
        $data['bus'] = $bus;
        $data['edit_mode'] = true;
        return view('admin.bus.form_bus', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bus  $bus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name' => 'required',
            'harga' => 'required',
            'diskon' => 'required',
            'all_in' => 'required',
            'seat' => 'required',
            'type' => 'required',
            'transmisi' => 'required',
            'p3k' => 'required',
            'audio' => 'required',
            'ac' => 'required',
            'charger' => 'required',
            'bensin' => 'required',
            'image' => 'file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'harga' => $request->harga,
            'diskon' => $request->diskon,
            'all_in' => $request->all_in,
            'seat' => $request->seat,
            'type' => $request->type,
            'transmisi' => $request->transmisi,
            'p3k' => $request->p3k,
            'audio' => $request->audio,
            'ac' => $request->ac,
            'charger' => $request->charger,
            'bensin' => $request->bensin,
        );

        $current = Bus::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('bus', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }
        $current->update($object);
        return redirect()->route('admin.bues.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bus  $bus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bus = Bus::where('id', $id)->firstOrFail();
        File::delete('./uploads/' . $bus->image);
        $bus->delete();
        return redirect()->route('admin.bues.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
