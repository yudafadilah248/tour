<?php

namespace App\Http\Controllers;

use App\Models\Cars;
use App\Models\Mices;
use App\Models\Profil;
use App\Models\Testimoni;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class MicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mice()
    {
        $testimoni = Testimoni::all();
        $mice = Mices::where('id', 1)
            ->first();
        $top_car = Cars::where('status', 1)
            ->get();
        $profil = Profil::where('id', 1)
            ->first();
        // dd($mice->toArray());
        $data['page_title'] = 'Kontak Travel';
        $data['mice'] = $mice;
        $data['profil'] = $profil;
        $data['top_car'] = $top_car;
        $data['testimoni'] = $testimoni;
        return view('mice', $data);
    }
    public function mice_event()
    {
        $testimoni = Testimoni::all();
        $mice_event = Mices::where('id', 1)
            ->first();
        $top_car = Cars::where('status', 1)
            ->get();
        $kontak = Profil::where('id', 1)
            ->first();
        $profil = Profil::where('id', 1)
            ->first();
        // dd($kontak->toArray());
        $data['page_title'] = 'Kontak Travel';
        $data['mice_event'] = $mice_event;
        $data['kontak'] = $kontak;
        $data['profil'] = $profil;
        $data['top_car'] = $top_car;
        $data['testimoni'] = $testimoni;
        return view('mice_event', $data);
    }

    public function index()
    {
        //
        // dd('aa');
        $id = 1;
        $mice = Mices::find($id);
        // dd($mice->toArray());
        $data['page_title'] = 'Manajemen Mice';
        $data['mice'] = $mice;
        return view('admin.mice.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah Mice';
        return view('admin.mice.form_mice', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'gathering' => 'required',
            'event' => 'required',
            'meeting' => 'required',
        );

        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $object = array(
            'gathering' => $request->gathering,
            'event' => $request->event,
            'meeting' => $request->meeting,
        );

        dd($object);

        Mices::create($object);
        return redirect()->route('admin.mice.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $mice = Mices::find($id);
        $data['page_title'] = 'Edit Mice';
        $data['mice'] = $mice;
        $data['edit_mode'] = true;
        return view('admin.mice.form_mice', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_mice(Request $request)
    {
        //
        $rules = array(
            'gathering' => 'required',
            'event' => 'required',
            'meeting' => 'required',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $object = array(
            'gathering' => $request->gathering,
            'event' => $request->event,
            'meeting' => $request->meeting,
        );

        $id = 1;

        $current = Mices::findOrFail($id);

        $current->update($object);
        return redirect()->route('admin.mice.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mice = Mices::where('id', $id)->firstOrFail();
        $mice->delete();
        return redirect()->route('admin.mice.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
