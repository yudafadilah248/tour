<?php

namespace App\Http\Controllers;

// use App\Models\auth;
use App\Models\user;
use Validator;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Mail\VerifyMail;

use App\Http\Requests\StoreauthRequest;
use App\Http\Requests\UpdateauthRequest;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Auth::user()->role);
        if (Auth::check() == false) {
            $data["header"] = 'Login';
            return view('admin.login', $data);
        } else {
            if (Auth::user()->role == 'admin') {
                return redirect('admin/dashboard');
            }
        }
        // //
        // $data['header'] = 'Login';
        // return view('admin/login', $data);
    }

    public function register()
    {
        $data['header'] = 'Register';
        return view('admin/register', $data);
    }

    public function proses_login(Request $request)
    {
        $validator = $request->validate([
            'email' => 'required',
            'password' => 'required|min:8'
        ], [
            'required' => 'The :attribute field is required.',
            'min' => 'the :attribute field is required 8 characters.',
        ]);
        if (Auth::attempt($validator)) {
            $user = Auth::user();
            if ($user->role == 'admin') {
                return redirect('admin/dashboard');
                // dd('aa');
            }
            return redirect('/')->with(['notif_status' => '0', 'notif' => 'User dan Password tidak sesuai.']);
        }
        return redirect('login')->with(['notif_status' => '0', 'notif' => 'User dan Password tidak sesuai.']);
    }


    public function proses_register(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'no_hp' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
        );
        $validator = Validator::make($request->all(), $rules,  $messages = [
            'required' => 'The :attribute field is required.',
            'min' => 'the :attribute field is required 8 characters.',
            'email.unique'           => 'The :attribute field already registered .',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $token = Str::random(60);
        $user = array(
            'name' => $request->name,
            'email' => $request->email,
            'role' => 'admin',
            'no_hp' => $request->no_hp,
            'password' => Hash::make($request->password),
            'image' => 0,
            'token' => $token,
        );

        // dd($user);
        User::create($user);

        Mail::to($request->email)->send(new VerifyMail($user, $token));
        return redirect('login');
    }

    public function verifyMail($token)
    {
        $user = User::where('token', $token)->first();
        $current = User::find($user->id);
        $object = array(
            'email_verified_at' => \Carbon\Carbon::now()
        );
        // dd($object);
        $current->update($object);
        return redirect()->route('login');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreauthRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreauthRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function show(auth $auth)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function edit(auth $auth)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateauthRequest  $request
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateauthRequest $request, auth $auth)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function destroy(auth $auth)
    {
        //
    }
}
