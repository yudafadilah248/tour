<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tour_Categories;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use App\Models\Tour_Price;
use App\Models\Tours;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class TourCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Tour_Categories::all();
        // dd($artikel->toArray());
        $data['page_title'] = 'Manajemen Kategori Tour';
        $data['categories'] = $categories;
        return view('admin.tour_categori.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah Kategori Tour';
        $data['edit_mode'] = false;
        return view('admin.tour_categori.form_categori', $data);
    }

    public function create_tour($id)
    {
        //
        $categori = Tour_Categories::find($id);
        $data['categori'] = $categori;
        $data['edit_mode'] = false;
        $data['page_title'] = 'Tambah Tour';
        return view('admin.tour_categori.form_tour', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'name' => 'required',
            'deskripsi' => 'required',
            'price' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );

        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'deskripsi' => $request->deskripsi,
            'price' => $request->price,
            'slug' => Str::slug($request->name, '-'),
        );

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('tour_categori', $request->image);
            $object['image'] = $image;
            // dd($object['image']);
        }
        // dd($object);

        Tour_Categories::create($object);
        return redirect()->route('admin.tour_categori.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    public function store_tour(Request $request, $id)
    {
        $categori = Tour_Categories::find($id);
        //
        $rules = array(
            'name' => 'required',
            'start_price' => 'required',
            'wisata' => 'required',
            'itenary' => 'required',
            'diskon' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );

        // dd($request->diskon);

        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // dd($object);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('tour', $request->image);
            // $object['image'] = $image;
            // dd($object['image']);
        }

        $tour_id = DB::table('tours')->insertGetId([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
            'start_price' => $request->start_price,
            'wisata' => $request->wisata,
            'itenary' => $request->itenary,
            'diskon' => $request->diskon,
            'categori_id' => $categori->id,
            'created_at' => date('Y-m-d H:i:s'),
            'image' => $image,
        ]);

        $jumlah = $request->jumlah;

        foreach ($jumlah as $key => $item) {
            $kategori[] = [
                'jumlah' => $request->jumlah[$key],
                'price' => $request->price[$key],
                'kategori' => 'dewasa',
                'tour_id' => $tour_id,
            ];
        }
        // dd($kategori);
        DB::table('tour_price')->insert($kategori);
        // Tours::create($object);
        return redirect()->route('admin.tour_categori.edit', ['tour_categori' => $categori->id])
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tour_Categories  $tour_Categories
     * @return \Illuminate\Http\Response
     */
    public function show(Tour_Categories $tour_Categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tour_Categories  $tour_Categories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categori = Tour_Categories::find($id);
        $tour = Tours::where('categori_id', $categori->id)->get();
        $data['page_title'] = 'Edit Kategori Tour';
        $data['categori'] = $categori;
        $data['tour'] = $tour;
        $data['edit_mode'] = true;
        return view('admin.tour_categori.form_categori', $data);
    }

    public function edit_tour($id)
    {
        //
        $tour = Tours::where('id', $id)
            ->with('tour_price')
            ->first();
        // dd($tour->toArray());
        $data['page_title'] = 'Edit Tour';
        $data['tour'] = $tour;
        $data['tour_price'] = $tour->tour_price;
        $data['edit_mode'] = true;
        return view('admin.tour_categori.form_tour', $data);
    }

    public function edit_price($id)
    {
        //
        $tour_price = Tour_Price::find($id);
        return response()->json([
            'data' => $tour_price
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tour_Categories  $tour_Categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name' => 'required',
            'deskripsi' => 'required',
            'price' => 'required',
            'image' => 'file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'price' => $request->price,
            'deskripsi' => $request->deskripsi,
            'slug' => Str::slug($request->name, '-'),
        );

        $current = Tour_Categories::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('tour_categori', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }

        $current->update($object);
        return redirect()->route('admin.tour_categori.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    public function update_tour(Request $request, $id)
    {
        //
        // dd('makan');
        $rules = array(
            'name' => 'required',
            'start_price' => 'required',
            'wisata' => 'required',
            'itenary' => 'required',
            'diskon' => 'required',
            'image' => 'file|mimes:jpg,png',
        );

        // dd($rules);
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
            'start_price' => $request->start_price,
            'wisata' => $request->wisata,
            'diskon' => $request->diskon,
            'itenary' => $request->itenary,
        );

        // dd($object);

        $current = Tours::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('galeri', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }


        $current->update($object);

        $jumlah = $request->jumlah;
        foreach ($jumlah as $key => $item) {
            $kategori = [
                'jumlah' => $request->jumlah[$key],
                'price' => $request->price[$key],
                'kategori' => 'dewasa',
                'tour_id' => $current->id
            ];
            DB::table('tour_price')->where('tour_id', $id)->updateOrInsert($kategori);
        }
        return redirect()->route('admin.tour_categori.edit', ['tour_categori' => $current->categori_id])
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    public function update_price(Request $request, $id)
    {
        //
        Tour_Price::updateOrCreate(
            [
                'id' => $id
            ],
            [
                'price' => $request->price,
                'jumlah' => $request->jumlah,
                'kategori' => 'dewasa',
            ]
        );
        return response()->json(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tour_Categories  $tour_Categories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categori = Tour_Categories::where('id', $id)->firstOrFail();
        $tour = Tours::where('categori_id', $categori->id)->firstOrFail();
        File::delete('./uploads/' . $categori->image);
        $categori->delete();
        File::delete('./uploads/' . $tour->image);
        $tour->delete();
        Tour_Price::where('tour_id', $tour->id)->delete();
        return redirect()->route('admin.tour_categori.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
    public function destroy_tour($id)
    {
        $tour = Tours::where('id', $id)->firstOrFail();
        File::delete('./uploads/' . $tour->image);
        Tour_Price::where('tour_id', $id)->delete();
        $tour->delete();
        return redirect()->route('admin.tour_categori.edit', ['tour_categori' => $tour->categori_id])
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
    public function destroy_price($id)
    {
        $tour = Tour_Price::where('id', $id)->firstOrFail();
        $tour->delete();
        return redirect()->route('admin.tour_categori.edit_tour', $tour->tour_id)
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
