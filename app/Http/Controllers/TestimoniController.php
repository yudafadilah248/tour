<?php

namespace App\Http\Controllers;

use App\Models\Testimoni;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Controllers\Controller;
use App\Models\Tour_Price;
use App\Models\Tours;

class TestimoniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $testimoni = Testimoni::all();
        $data['page_title'] = 'Manajemen Testimoni';
        $data['testimoni'] = $testimoni;
        // dd($testimoni->toArray());
        return view('admin.testimoni.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah Testimoni';
        return view('admin.testimoni.form_testimoni', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'name' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'deskripsi' => $request->deskripsi,
        );

        // dd($object);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('testimoni', $request->image);
            $object['image'] = $image;
            // dd($object['image']);
        }

        Testimoni::create($object);
        return redirect()->route('admin.testimoni.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Testimoni  $testimoni
     * @return \Illuminate\Http\Response
     */
    public function show(Testimoni $testimoni)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testimoni  $testimoni
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $testimoni = Testimoni::find($id);
        // dd($testimoni->toArray());
        $data['page_title'] = 'Edit Testimoni';
        $data['testimoni'] = $testimoni;
        $data['edit_mode'] = true;
        return view('admin.testimoni.form_testimoni', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Testimoni  $testimoni
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'deskripsi' => $request->deskripsi,
        );

        $current = Testimoni::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('testimoni', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }
        $current->update($object);
        return redirect()->route('admin.testimoni.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Testimoni  $testimoni
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
        $testimoni = Testimoni::where('id', $id)->firstOrFail();
        File::delete('./uploads/' . $testimoni->image);
        $testimoni->delete();
        return redirect()->route('admin.testimoni.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
