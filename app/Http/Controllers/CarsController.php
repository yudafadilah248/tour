<?php

namespace App\Http\Controllers;

use App\Models\Cars;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Controllers\Controller;
use App\Models\Profil;
use App\Models\Testimoni;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mobil()
    {
        //
        $mobil = Cars::all();
        $testimoni = Testimoni::all();
        $profil = Profil::where('id', 1)
            ->first();
        $data['mobil'] = $mobil;
        $data['testimoni'] = $testimoni;
        $data['profil'] = $profil;
        $data['page_title'] = 'Sewa Mobil';
        return view('sewa_mobil', $data);
    }

    public function index()
    {
        //
        $car = Cars::all();
        // dd($galeri->toArray());
        $data['page_title'] = 'Manajemen Mobil';
        $data['car'] = $car;
        return view('admin.car.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah Mobil';
        return view('admin.car.form_car', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'name' => 'required',
            'harga' => 'required',
            // 'diskon' => 'required',
            // 'kunci_12' => 'required',
            // 'kunci_full' => 'required',
            // 'sopir_12' => 'required',
            // 'sopir_full' => 'required',
            // 'all_12' => 'required',
            // 'all_full' => 'required',
            'seat' => 'required',
            'type' => 'required',
            'transmisi' => 'required',
            'p3k' => 'required',
            'audio' => 'required',
            'ac' => 'required',
            'status' => 'required',
            'charger' => 'required',
            'bensin' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'harga' => $request->harga,
            'diskon' => 0,
            'kunci_12' => $request->kunci_12,
            'kunci_full' => $request->kunci_full,
            'sopir_12' => $request->sopir_12,
            'sopir_full' => $request->sopir_full,
            'all_12' => $request->all_12,
            'all_full' => $request->all_full,
            'seat' => $request->seat,
            'type' => $request->type,
            'transmisi' => $request->transmisi,
            'p3k' => $request->p3k,
            'audio' => $request->audio,
            'status' => $request->audio,
            'ac' => $request->ac,
            'charger' => $request->charger,
            'bensin' => $request->bensin,
        );

        // dd($object);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('car', $request->image);
            $object['image'] = $image;
            // dd($object['image']);
        }

        Cars::create($object);
        return redirect()->route('admin.cars.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $car = Cars::find($id);
        // dd($galeri->toArray());
        $data['page_title'] = 'Edit Mobil';
        $data['car'] = $car;
        $data['edit_mode'] = true;
        return view('admin.car.form_car', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name' => 'required',
            'harga' => 'required',
            // 'diskon' => 'required',
            // 'kunci_12' => 'required',
            // 'kunci_full' => 'required',
            // 'sopir_12' => 'required',
            // 'sopir_full' => 'required',
            // 'all_12' => 'required',
            // 'all_full' => 'required',
            'seat' => 'required',
            'type' => 'required',
            'transmisi' => 'required',
            'p3k' => 'required',
            'audio' => 'required',
            'ac' => 'required',
            'charger' => 'required',
            'bensin' => 'required',
            'image' => 'file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'update data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'harga' => $request->harga,
            'diskon' => 0,
            'kunci_12' => $request->kunci_12,
            'kunci_full' => $request->kunci_full,
            'sopir_12' => $request->sopir_12,
            'sopir_full' => $request->sopir_full,
            'all_12' => $request->all_12,
            'all_full' => $request->all_full,
            'seat' => $request->seat,
            'type' => $request->type,
            'transmisi' => $request->transmisi,
            'p3k' => $request->p3k,
            'audio' => $request->audio,
            'status' => $request->status,
            'ac' => $request->ac,
            'charger' => $request->charger,
            'bensin' => $request->bensin,
        );

        $current = Cars::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('car', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }
        $current->update($object);
        return redirect()->route('admin.cars.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Cars::where('id', $id)->firstOrFail();
        File::delete('./uploads/' . $car->image);
        $car->delete();
        return redirect()->route('admin.cars.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
