<?php

namespace App\Http\Controllers;

use App\Models\Profil;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use Symfony\Component\HttpKernel\Profiler\Profile;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $id = 1;
        $profil = Profil::find($id);
        // dd($profil->toArray());
        $data['page_title'] = 'Manajemen profil';
        $data['profil'] = $profil;
        return view('admin.profil.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function show(Profil $profil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function edit(Profil $profil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function update_profil(Request $request, Profil $profil)
    {
        //
        $rules = array(
            'name' => 'required',
            'deskripsi_singkat' => 'required',
            'deskripsi' => 'required',
            'kontak' => 'required',
            'text_wa' => 'required',
            'ig' => 'required',
            'fb' => 'required',
            'alamat' => 'required',
            'maps' => 'required',
            'visi' => 'required',
            'misi' => 'required',
            'email' => 'required',
            'senin' => 'required',
            'selasa' => 'required',
            'rabu' => 'required',
            'kamis' => 'required',
            'jumat' => 'required',
            'sabtu' => 'required',
            'minggu' => 'required',
            'image' => 'file|mimes:jpg,png',
        );

        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
            'deskripsi_singkat' => $request->deskripsi_singkat,
            'deskripsi' => $request->deskripsi,
            'kontak' => $request->kontak,
            'text_wa' => $request->text_wa,
            'ig' => $request->ig,
            'fb' => $request->fb,
            'alamat' => $request->alamat,
            'maps' => $request->maps,
            'visi' => $request->visi,
            'misi' => $request->misi,
            'email' => $request->email,
            'senin' => $request->senin,
            'selasa' => $request->selasa,
            'rabu' => $request->rabu,
            'kamis' => $request->kamis,
            'jumat' => $request->jumat,
            'sabtu' => $request->sabtu,
            'minggu' => $request->minggu,
        );


        $id = 1;

        $current = Profil::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('profil', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }

        // dd($object);
        $current->update($object);
        return redirect()->route('admin.profil.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }
    public function update(Request $request, Profil $profil)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profil $profil)
    {
        //
    }
}
