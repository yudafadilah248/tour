<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Profil;
use App\Models\Testimoni;
use App\Models\Tour_Categories;
use App\Models\Tour_Price;
use App\Models\Tours;

class WisataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $clientIP = request()->ip();
        // dd($clientIP);
        $testimoni = Testimoni::all();
        // $wisata = Tour_Categories::all();
        $wisata = Tour_Categories::withCount('tour')->get();
        // $tour = Tours::with('tour_categori')->get();
        // $tour = Tours::select('categori_id', DB::raw('count(*) as total'))
        //     ->groupBy('categori_id')
        //     ->get();
        // dd($wisata->toArray());
        $top_car = Cars::where('status', 1)
            ->get();
        $profil = Profil::where('id', 1)
            ->first();
        $data['page_title'] = 'Paket Wisata';
        $data['testimoni'] = $testimoni;
        $data['wisata'] = $wisata;
        $data['profil'] = $profil;
        $data['top_car'] = $top_car;
        // $data['tour'] = $tour;
        return view('paket_wisata', $data);
    }

    public function wisata($slug)
    {
        $blog = DB::table('artikels')
            ->where('status', 1)
            ->paginate(6);
        $testimoni = Testimoni::all();
        $paket = Tour_Categories::all();
        $wisata = Tour_Categories::where('slug', $slug)
            ->first();

        $top_car = Cars::where('status', 1)
            ->get();

        $tour = Tours::where('categori_id', $wisata->id)
            ->with('tour_categori', 'tour_price')
            ->get();
        // with('tour_price')
        //     ->whereHas('tour_price', function ($q) use ($a) {
        //         $q->where('kategori', '=', $a);
        //     })
        $profil = Profil::where('id', 1)
            ->first();

        $data['page_title'] = 'Detail Wisata';
        $data['tour'] = $tour;
        $data['wisata'] = $wisata;
        $data['top_car'] = $top_car;
        $data['blog'] = $blog;
        $data['profil'] = $profil;
        $data['testimoni'] = $testimoni;
        $data['paket'] = $paket;
        return view('wisata', $data);
    }

    public function detail_wisata($slug)
    {
        $blog = DB::table('artikels')
            ->where('status', 1)
            ->paginate(6);
        $testimoni = Testimoni::all();
        $paket = Tour_Categories::all();
        $wisata = Tour_Categories::where('slug', $slug)
            ->first();
        $top_car = Cars::where('status', 1)
            ->get();
        $profil = Profil::where('id', 1)
            ->first();

        $tour = Tours::where('slug', $slug)
            ->with('tour_categori', 'tour_price')
            ->first();
        // dd($tour->toArray());

        $d_anak = Tours::where('id', $tour->id)
            ->with('tour_price', function ($query) {
                $query->where('kategori', '=', 'anak')
                    ->orderBy('id', 'asc');
            })
            ->first();

        $d_balita = Tours::where('id', $tour->id)
            ->with('tour_price', function ($query) {
                $query->where('kategori', '=', 'balita')->orderBy('id', 'asc');
            })
            ->first();
        $d_dewasa = Tours::where('id', $tour->id)
            ->with('tour_price', function ($query) {
                $query->where('kategori', '=', 'dewasa')->orderBy('id', 'asc');
            })
            ->first();

        $data['page_title'] = 'Detail Wisata';
        $data['tour'] = $tour;
        $data['wisata'] = $wisata;
        $data['top_car'] = $top_car;
        $data['d_anak'] = $d_anak;
        $data['d_dewasa'] = $d_dewasa;
        $data['d_balita'] = $d_balita;
        $data['blog'] = $blog;
        $data['profil'] = $profil;
        $data['testimoni'] = $testimoni;
        $data['paket'] = $paket;
        return view('detail_wisata', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
