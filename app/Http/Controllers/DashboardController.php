<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Models\dashboard;
use App\Models\artikel;
use App\Models\Cars;
use App\Models\Tour_Categories;
use App\Models\Tours;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Nette\Utils\Callback;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $from = date("Y-m-d h:i:s", strtotime("-31 days"));
        // dd($from);
        $to = date("Y-m-d h:i:s");

        $car_date = Cars::whereBetween('created_at', [$from, $to])->get();

        $kategori_date = Tour_Categories::whereBetween('created_at', [$from, $to])->get();

        $tour_date = Tours::whereBetween('created_at', [$from, $to])->get();

        $artikel_date = artikel::whereBetween('created_at', [$from, $to])->get();

        $car = Cars::all();
        $artikel = artikel::all();
        $kategori = Tour_Categories::all();
        $tour = Tours::all();
        $top_car = Cars::where('status', 1)
            ->get();
        $data['car'] = count($car);
        $data['artikel'] = count($artikel);
        $data['kategori'] = count($kategori);
        $data['tour'] = count($tour);
        $data['car_date'] = count($car_date);
        $data['kategori_date'] = count($kategori_date);
        $data['tour_date'] = count($tour_date);
        $data['artikel_date'] = count($artikel_date);
        $data['top_car'] = $top_car;

        $data['page_title'] = 'Dashboard';
        return view('admin.dashboard.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoredashboardRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoredashboardRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatedashboardRequest  $request
     * @param  \App\Models\dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatedashboardRequest $request, dashboard $dashboard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(dashboard $dashboard)
    {
        //
    }
}
