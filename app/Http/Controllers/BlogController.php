<?php

namespace App\Http\Controllers;

use App\Models\artikel;
use App\Models\Cars;
use App\Models\Profil;
use App\Models\Testimoni;
use App\Models\Tour_Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blog = DB::table('artikels')
            ->where('status', 1)
            ->paginate(6);
        $testimoni = Testimoni::all();
        $paket = Tour_Categories::all();
        $top_car = Cars::where('status', 1)
            ->get();
        $profil = Profil::where('id', 1)
            ->first();
        $data['page_title'] = 'Blog Travel ';
        $data['blog'] = $blog;
        $data['profil'] = $profil;
        $data['top_car'] = $top_car;
        $data['testimoni'] = $testimoni;
        $data['paket'] = $paket;
        // $data['galeri'] = $galeri;
        return view('blog', $data);
    }
    public function detail_blog($slug)
    {
        //
        $detail = artikel::where('slug', $slug)->first();
        $blog = DB::table('artikels')
            ->where('status', 1)
            ->limit(6)
            ->orderBy('id', 'DESC')
            ->get();
        $top_car = Cars::where('status', 1)
            ->get();
        $paket = Tour_Categories::all();
        $profil = Profil::where('id', 1)
            ->first();
        // dd($blog->toArray());
        $testimoni = Testimoni::all();
        $data['page_title'] = 'Detail Blog Travel ';
        $data['detail'] = $detail;
        $data['blog'] = $blog;
        $data['paket'] = $paket;
        $data['top_car'] = $top_car;
        $data['profil'] = $profil;
        $data['testimoni'] = $testimoni;
        // $data['galeri'] = $galeri;
        return view('detail_blog', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
