<?php

namespace App\Http\Controllers;

use App\Models\Conditions;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Controllers\Controller;

class ConditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
            $id = 1;
            $condition = Conditions::find($id);
        // dd($condition->toArray());
        $data['page_title'] = 'Manajemen Condition';
        $data['condition'] = $condition;
        return view('admin.condition.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah Condition';
        return view('admin.condition.form_condition', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'domisili' => 'required',
            'luar_domisili' => 'required',
            'mahasiswa' => 'required',
            'foreigner' => 'required',
        );

        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $object = array(
            'domisili' => $request->domisili,
            'luar_domisili' => $request->luar_domisili,
            'mahasiswa' => $request->mahasiswa,
            'foreigner' => $request->foreigner,
        );

        // dd($object);

        Conditions::create($object);
        return redirect()->route('admin.condition.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function show(Conditions $conditions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $condition = Conditions::find($id);
        $data['page_title'] = 'Edit Condition';
        $data['condition'] = $condition;
        $data['edit_mode'] = true;
        return view('admin.condition.form_condition', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function update_condition(Request $request)
    {
        //
        $rules = array(
            'domisili' => 'required',
            'luar_domisili' => 'required',
            'mahasiswa' => 'required',
            'foreigner' => 'required',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $object = array(
            'domisili' => $request->domisili,
            'luar_domisili' => $request->luar_domisili,
            'mahasiswa' => $request->mahasiswa,
            'foreigner' => $request->foreigner,
        );

        $id = 1;

        $current = Conditions::findOrFail($id);

        $current->update($object);
        return redirect()->route('admin.condition.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $condition = Conditions::where('id', $id)->firstOrFail();
        $condition->delete();
        return redirect()->route('admin.condition.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
