<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Conditions;
use App\Models\Profil;
use App\Models\Testimoni;
use App\Models\Tour_Categories;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimoni = Testimoni::all();
        $top_car = Cars::where('status', 1)
            ->get();
        $diskon_car = Cars::where('diskon', '!=', 0)
            ->where('status', '!=', 1)
            ->get();
        $wisata = Tour_Categories::withCount('tour')
            ->limit(3)
            ->orderBy('id', 'ASC')
            ->get();
        $profil = Profil::where('id', 1)
            ->first();
        $syarat = Conditions::where('id', 1)
            ->first();
        // dd($diskon_car->toArray());
        $data['page_title'] = 'Home';
        $data['testimoni'] = $testimoni;
        $data['top_car'] = $top_car;
        $data['diskon_car'] = $diskon_car;
        $data['wisata'] = $wisata;
        $data['syarat'] = $syarat;
        $data['profil'] = $profil;
        // $data['galeri'] = $galeri;
        return view('home', $data);
    }
    public function home()
    {
        $testimoni = Testimoni::all();
        $top_car = Cars::where('status', 1)
            ->get();
        $diskon_car = Cars::where('diskon', '!=', 0)
            ->where('status', '!=', 1)
            ->get();
        $wisata = Tour_Categories::withCount('tour')
            ->limit(3)
            ->orderBy('id', 'ASC')
            ->get();
        $profil = Profil::where('id', 1)
            ->first();
        // dd($wisata->toArray());
        $data['page_title'] = ' ';
        $data['testimoni'] = $testimoni;
        $data['top_car'] = $top_car;
        $data['diskon_car'] = $diskon_car;
        $data['wisata'] = $wisata;
        $data['profil'] = $profil;
        // $data['galeri'] = $galeri;
        return view('home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
