<?php

namespace App\Http\Controllers;

use App\Models\Galeri;
use Illuminate\Http\Request;
use Validator;
use File;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Profil;
use App\Models\Testimoni;
use App\Models\Tour_Price;
use App\Models\Tours;

class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function galeri()
    {
        //
        $galeri = Galeri::paginate(9);
        $testimoni = Testimoni::all();
        $profil = Profil::where('id', 1)
            ->first();
        $top_car = Cars::where('status', 1)
            ->get();
        $diskon_car = Cars::where('diskon', '!=', 0)
            ->where('status', '!=', 1)
            ->get();
        // dd($diskon_car->toArray());
        $data['page_title'] = 'Galeri Travel';
        $data['galeri'] = $galeri;
        $data['profil'] = $profil;
        $data['top_car'] = $top_car;
        $data['testimoni'] = $testimoni;
        // dd($galeri->toArray());
        return view('galeri', $data);
    }

    public function index()
    {
        //
        $galeri = Galeri::all();
        // dd($galeri->toArray());
        $data['page_title'] = 'Manajemen Galeri';
        $data['galeri'] = $galeri;
        return view('admin.galeri.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['page_title'] = 'Tambah Galeri';
        return view('admin.galeri.form_galeri', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'name' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
        );

        // dd($object);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('galeri', $request->image);
            $object['image'] = $image;
            // dd($object['image']);
        }

        Galeri::create($object);
        return redirect()->route('admin.galeri.index')
            ->with(['notif_status' => '1', 'notif' => 'Insert data succed.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function show(Galeri $galeri)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $galeri = Galeri::find($id);
        // dd($galeri->toArray());
        $data['page_title'] = 'Edit Galeri';
        $data['galeri'] = $galeri;
        $data['edit_mode'] = true;
        return view('admin.galeri.form_galeri', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = array(
            'name' => 'required',
            'image' => 'required|file|mimes:jpg,png',
        );
        $validator = Validator::make($request->all(), $rules, $messages = [
            'required' => 'The :attribute field is required.',
            'file' => 'The :attribute must be a file.',
            'mimes' => 'The :attribute must be a file of type: :values.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->with(['notif_status' => '0', 'notif' => 'Insert data failed.'])
                ->withInput();
        }

        $object = array(
            'name' => $request->name,
        );

        $current = Galeri::findOrFail($id);

        if ($request->has('image')) {
            $image = Storage::disk('uploads')->put('galeri', $request->image);
            $object['image'] = $image;
            if ($current->image) {
                File::delete('./uploads/' . $current->image);
            }
        }
        $current->update($object);
        return redirect()->route('admin.galeri.index')
            ->with(['notif_status' => '1', 'notif' => 'Update data succeed.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Galeri  $galeri
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $galeri = Galeri::where('id', $id)->firstOrFail();
        File::delete('./uploads/' . $galeri->image);
        $galeri->delete();
        return redirect()->route('admin.galeri.index')
            ->with(['notif_status' => '1', 'notif' => 'Delete data succeed.']);
    }
}
