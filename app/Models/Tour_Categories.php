<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tour_Categories extends Model
{
    use HasFactory;
    protected $table = "tour_categories";
    protected $fillable = array(
        'name',
        'deskripsi',
        'image',
        'price',
        'slug',
    );

    public function tour()
    {
        return $this->hasMany(Tours::class, 'categori_id', 'id');
    }
}
