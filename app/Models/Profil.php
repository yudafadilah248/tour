<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    use HasFactory;
    protected $table = "profil";
    protected $fillable = array(
        'name',
        'deskripsi_singkat',
        'deskripsi',
        'kontak',
        'text_wa',
        'ig',
        'fb',
        'alamat',
        'maps',
        'visi',
        'misi',
        'email',
        'image',
        'senin',
        'selasa',
        'rabu',
        'kamis',
        'jumat',
        'sabtu',
        'minggu',
    );
}
