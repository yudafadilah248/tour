<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    use HasFactory;
    protected $table = "bus";
    protected $fillable = array(
        'name',
        'harga',
        'image',
        'diskon',
        'all_in',
        'seat',
        'type',
        'transmisi',
        'p3k',
        'audio',
        'ac',
        'charger',
        'bensin',
        'status',
    );
}
