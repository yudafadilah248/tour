<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tours extends Model
{
    use HasFactory;
    protected $table = "tours";
    protected $fillable = array(
        'name',
        'slug',
        'wisata',
        'image',
        'itenary',
        'start_price',
        'categori_id',
        'diskon',
    );


    public function tour_price()
    {
        return $this->hasMany(Tour_Price::class, 'tour_id', 'id');
    }
    public function tour_categori()
    {
        return $this->hasOne(Tour_Categories::class, 'id', 'categori_id');
    }
}
