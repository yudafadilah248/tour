<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tour_Price extends Model
{
    use HasFactory;
    protected $table = "tour_price";
    protected $fillable = array(
        'kategori',
        'price',
        'jumlah',
        'tour_id',
    );
}
