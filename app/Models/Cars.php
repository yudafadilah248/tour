<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    use HasFactory;
    protected $table = "cars";
    protected $fillable = array(
        'name',
        'harga',
        'image',
        'diskon',
        'kunci_12',
        'sopir_12',
        'all_12',
        'kunci_full',
        'sopir_full',
        'all_full',
        'seat',
        'type',
        'transmisi',
        'p3k',
        'audio',
        'ac',
        'charger',
        'bensin',
        'status',
    );
}
