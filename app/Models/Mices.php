<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mices extends Model
{
    use HasFactory;
    protected $table = "mices";
    protected $fillable = array(
        'meeting',
        'event',
        'gathering',
    );
}
