<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conditions extends Model
{
    use HasFactory;
    protected $table = "conditions";
    protected $fillable = array(
        'domisili',
        'luar_domisili',
        'mahasiswa',
        'foreigner',
    );
}
