-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2022 at 08:47 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tour`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikels`
--

CREATE TABLE `artikels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kutipan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artikel` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `artikels`
--

INSERT INTO `artikels` (`id`, `judul`, `image`, `slug`, `kutipan`, `status`, `artikel`, `tanggal`, `created_at`, `updated_at`) VALUES
(1, 'Lorem ipsum, atau ringkasnya lipsum1', 'artikel/TCAg76WL1mwuSm4MoWGy6yVART21akikrTTxXt25.jpg', 'lorem-ipsum-atau-ringkasnya-lipsum1', 'Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasii.', '1', '<p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p><p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p>', '2022-08-11', '2022-08-09 01:06:04', '2022-09-29 07:53:47'),
(2, 'Lorem ipsum, atau ringkasnya lipsum2', 'artikel/FEkXLJr3yAVfjapnIOTWzOs2kS4EhShg2TUKBqHs.jpg', 'lorem-ipsum-atau-ringkasnya-lipsum2', 'Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak.', '1', '<p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p><p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p>', '2022-09-29', '2022-09-29 07:33:37', '2022-09-29 07:53:57'),
(3, 'yu yuda', 'artikel/EUf9DeMz0F4NvwdMkiLWYvEgfPVQ9i6dsGX5BxNv.jpg', 'yu-yuda', 'Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font.', '1', '<p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p><p>&nbsp;</p><p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p>', '2022-09-29', '2022-09-29 07:34:24', '2022-09-29 07:34:24'),
(4, 'Lorem ipsum, atau ringkasnya lipsum', 'artikel/xy1ENyLj2adN9bm9unJaiwU1bu7d34JDA6wzXlFy.png', 'lorem-ipsum-atau-ringkasnya-lipsum', 'Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, d', '1', '<p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p><p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p><p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p><p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p>', '2022-09-29', '2022-09-29 07:34:52', '2022-09-29 07:54:09');

-- --------------------------------------------------------

--
-- Table structure for table `bus`
--

CREATE TABLE `bus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` int(11) NOT NULL,
  `all_in` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('HD','UHD','SHD','HDD','ELF','HIACE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `transmisi` enum('manual','autmatic') COLLATE utf8mb4_unicode_ci NOT NULL,
  `p3k` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `charger` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `bensin` enum('pertalite','pertamax','solar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bus`
--

INSERT INTO `bus` (`id`, `name`, `harga`, `image`, `diskon`, `all_in`, `seat`, `type`, `transmisi`, `p3k`, `audio`, `ac`, `charger`, `bensin`, `created_at`, `updated_at`) VALUES
(2, '12111111111111111', '12', 'bus/oz3CqCAMCp0JXcBZaOsDZ0m8AuwuoR9HE0knuPgM.jpg', 12, '121111111', '12', 'HD', 'manual', '1', '1', '1', '1', 'pertalite', '2022-09-28 23:48:45', '2022-09-29 00:07:49');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` int(11) NOT NULL,
  `kunci_12` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kunci_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sopir_12` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sopir_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all_12` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('hathback','city','mpv') COLLATE utf8mb4_unicode_ci NOT NULL,
  `transmisi` enum('manual','autmatic') COLLATE utf8mb4_unicode_ci NOT NULL,
  `p3k` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `charger` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `bensin` enum('pertalite','pertamax','solar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `name`, `harga`, `image`, `diskon`, `kunci_12`, `kunci_full`, `sopir_12`, `sopir_full`, `all_12`, `all_full`, `seat`, `type`, `transmisi`, `p3k`, `audio`, `ac`, `charger`, `bensin`, `created_at`, `updated_at`) VALUES
(1, 'Yuda1', '90001', 'car/jRmIwo8eOlZm7KgYihLdYqVmmYPjE2dBY5ryrxaa.png', 501, '122', '12', '12', '12', '12', '12', '501111', 'hathback', 'manual', '0', '0', '0', '0', 'pertalite', '2022-09-25 09:34:29', '2022-09-25 09:41:20');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `domisili` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `luar_domisili` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mahasiswa` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreigner` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `domisili`, `luar_domisili`, `mahasiswa`, `foreigner`, `created_at`, `updated_at`) VALUES
(1, '<ul><li>2 KTP Asli dan masih berlaku.</li><li>SIM C Asli dan masih berlaku</li><li>1 Unit Motor tahun 2014 keatas</li><li>beserta STNK (pajak aktif).</li><li>Bersedia menunjukkan SIM A dan masih berlaku.</li><li>Bersedia untuk diambil gambar atau foto.</li></ul>', '<ul><li>2 KTP Asli dan masih berlaku.</li><li>SIM C Asli dan masih berlaku</li><li>1 Unit Motor tahun 2014 keatas</li><li>beserta STNK (pajak aktif).</li><li>Bersedia menunjukkan SIM A dan masih berlaku.</li><li>Bersedia untuk diambil gambar atau foto.</li></ul>', '<ul><li>2 KTP Asli dan masih berlaku.</li><li>SIM C Asli dan masih berlaku</li><li>1 Unit Motor tahun 2014 keatas</li><li>beserta STNK (pajak aktif).</li><li>Bersedia menunjukkan SIM A dan masih berlaku.</li><li>Bersedia untuk diambil gambar atau foto.</li></ul>', '<ul><li>2 KTP Asli dan masih berlaku.</li><li>SIM C Asli dan masih berlaku</li><li>1 Unit Motor tahun 2014 keatas</li><li>beserta STNK (pajak aktif).</li><li>Bersedia menunjukkan SIM A dan masih berlaku.</li><li>Bersedia untuk diambil gambar atau foto.</li></ul>', '2022-09-14 01:08:08', '2022-09-30 09:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `dashboards`
--

CREATE TABLE `dashboards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Ashiap', 'galeri/xK7bEMdMNu9FaTynKAaI5x2jJIxwHbaD78xG1UOn.jpg', '2022-09-14 00:41:10', '2022-09-29 00:31:17'),
(2, 'kado', 'galeri/5YacykpyZeSBRHlordx5Q3FuZfucs3bQSK0gcdnq.jpg', '2022-09-29 00:12:03', '2022-09-29 00:31:52'),
(3, 'cipung', 'galeri/lgEzJhPqP47pLnQEAQqUn13UkwHvSlTf0cAtp0ap.jpg', '2022-09-29 00:12:16', '2022-09-29 00:12:16'),
(4, 'keripik', 'galeri/INQ4qbzlqYodYHN2GfwM50HAbemCF1xEpEAoazC4.png', '2022-09-29 00:17:21', '2022-09-29 00:17:21'),
(5, 'abcd', 'galeri/47WcoACqCShBh2S1PpW5OFJ8thSbDNhABvQEKA9r.png', '2022-09-29 02:16:15', '2022-09-29 02:16:15'),
(6, '1234', 'galeri/t561EJgOG1PuykHTFJ6I7pgBxMuqc2E6aJ1y46xG.jpg', '2022-09-29 02:16:30', '2022-09-29 02:16:30'),
(7, 'qazxsw', 'galeri/yQzLB7CgyzXNuZKPQFKjDW74pJPMGC9Q7nT8kcBy.jpg', '2022-09-29 02:16:47', '2022-09-29 02:16:47'),
(8, '12', 'galeri/vh7xf50L1FP3M4gTDArZn8dJqmFQM7z3B2bYfr42.jpg', '2022-09-29 02:17:23', '2022-09-29 02:17:23'),
(9, '12', 'galeri/lFQpxaKNnHfXJWhqgIa2VYizYpKmMR6GxwxIdhk5.jpg', '2022-09-29 02:18:02', '2022-09-29 02:18:02'),
(10, '12', 'galeri/EwYzejK8xeGj4lYCz3DkXNSm0nl8hMTFvITJmO0U.jpg', '2022-09-29 02:18:14', '2022-09-29 02:18:14'),
(11, 'abcd', 'galeri/f79cI3coE3I5mXdzXVsATWSdpoY2nhNdDRIY71TA.jpg', '2022-09-29 02:19:11', '2022-09-29 02:19:11'),
(12, 'abcd', 'galeri/Y2GLQ1N5opJo1DxLS8gqWmGr6PvP5AkSqlHxUG2n.jpg', '2022-09-29 02:19:27', '2022-09-29 02:19:27'),
(13, 'abcd', 'galeri/NdYW555uTlAg6Di3xjb1YFqivJfDKpTvqGFZpMQq.jpg', '2022-09-29 02:19:28', '2022-09-29 02:19:28'),
(14, 'abcd', 'galeri/VwAC7W5tH1EAxZwfZ8IGodHWghDJqiwIxjhFVxOs.jpg', '2022-09-29 02:19:47', '2022-09-29 02:19:47'),
(15, 'abcd', 'galeri/viHjOo6wcwa2YQNNsdnoFUOjS6eHxC3IVJE8dkWs.jpg', '2022-09-29 02:20:02', '2022-09-29 02:20:02'),
(16, 'cipung', 'galeri/ARIBIJ5NQDMTRBWvHHrTvr51cxfCy2L2OQs9qyTw.jpg', '2022-09-29 02:20:18', '2022-09-29 02:20:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(32, '2014_10_12_000000_create_users_table', 1),
(33, '2014_10_12_100000_create_password_resets_table', 1),
(34, '2019_08_19_000000_create_failed_jobs_table', 1),
(35, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(36, '2022_08_05_043302_create_dashboards_table', 1),
(38, '2022_08_09_064108_create_artikels_table', 2),
(46, '2022_09_12_071327_create_tour_price_table', 5),
(48, '2022_09_14_072448_create_galeri_table', 7),
(49, '2022_09_14_074747_create_conditions_table', 8),
(50, '2022_09_14_124201_create_profil_table', 9),
(51, '2022_09_10_101625_create_cars_table', 10),
(52, '2022_09_10_102059_create_bus_table', 11),
(53, '2022_09_29_073516_create_testimonis_table', 12),
(54, '2022_09_12_022209_create_tours_table', 13),
(55, '2022_09_12_022357_create_tour_categories_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi_singkat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kontak` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ig` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `maps` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `misi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `name`, `deskripsi_singkat`, `deskripsi`, `kontak`, `ig`, `fb`, `alamat`, `maps`, `visi`, `misi`, `email`, `image`, `created_at`, `updated_at`) VALUES
(1, 'YUda', '11', '<p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>. Maksud penggunaan lipsum adalah agar pengamat tidak terlalu berkonsentrasi kepada arti harfiah per kalimat, melainkan lebih kepada elemen desain dari teks yang dipresentasi.</p><p><strong>Lorem ipsum</strong>, atau ringkasnya <strong>lipsum</strong>, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti <a href=\"https://id.wikipedia.org/wiki/Font\"><i>font</i></a>, <a href=\"https://id.wikipedia.org/wiki/Tipografi\">tipografi</a>, dan <a href=\"https://id.wikipedia.org/wiki/Tata_letak\">tata letak</a>..</p>', '089674555439', '1', '1', '1', '1', '<ul><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li></ul>', '<ul><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li><li>Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan&nbsp;</li></ul>', 'yudafadilah248@gmail.com', 'profil/QUvKcEmgPkAHBhXYThmQwBexwqtsQd2mhTD0ivNS.jpg', NULL, '2022-09-30 09:48:16');

-- --------------------------------------------------------

--
-- Table structure for table `testimonis`
--

CREATE TABLE `testimonis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonis`
--

INSERT INTO `testimonis` (`id`, `name`, `deskripsi`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Lorem', 'Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi,', 'testimoni/7YQAGxzUk5MKBZsEYCHQkdwzBFreVQvSabDR06Kd.png', '2022-09-29 01:33:32', '2022-09-29 01:33:32'),
(3, 'abcd', 'Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi,', 'testimoni/mMZ6YCbnbCYOzrrwvcoDcgshJHXwDIP84Fv0F7Wr.png', '2022-09-29 01:41:19', '2022-09-29 01:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE `tours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categori_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wisata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `itenary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`id`, `categori_id`, `name`, `wisata`, `itenary`, `image`, `start_price`, `created_at`, `updated_at`) VALUES
(1, 1, 'paket 1', '<ul><li>1</li><li>1</li><li>1</li><li>1</li><li>1</li></ul>', '<ul><li>1</li><li>1</li><li>1</li><li>1</li><li>1</li></ul>', 'galeri/WOmxkWP7PSLrhL23VklnhgDGJyf4Ho1vmBCW1bUr.jpg', '5000', NULL, '2022-09-29 20:17:36'),
(2, 2, '12', '<ul><li>1</li><li>1</li><li>1</li><li>1</li></ul>', '<p>Hari Pertama</p><ul><li>2</li><li>2</li><li>2</li><li>2</li></ul>', 'tour/vPHoswBprOH6XfqNNFal4e2BsF8GMx5E9PYGiJvP.jpg', '12', NULL, '2022-09-30 09:17:03'),
(3, 2, 'aa', '<ul><li>1</li><li>1</li><li>1</li><li>1</li></ul>', '<ul><li>2</li><li>2</li><li>2</li><li>2</li></ul>', 'tour/yWCWKns7MtDpUv3EVu20krbxII3AiDdn7QhuP5Th.jpg', '20000', NULL, NULL),
(4, 2, 'Paket 3', '<ul><li>1</li><li>1</li><li>1</li><li>1</li></ul>', '<ul><li>2</li><li>2</li><li>2</li><li>2</li></ul>', 'tour/P1DwS3pgVj3regdfggqxAl9my0YCMik4JMaW20MO.jpg', '5000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tour_categories`
--

CREATE TABLE `tour_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tour_categories`
--

INSERT INTO `tour_categories` (`id`, `name`, `deskripsi`, `image`, `price`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Wisata Jogja 1 hari.', 'Paket Tour Jogja 1 hari.', 'tour_categori/Eznkja08igN39XYrlhv3SQc3kMfW7P8fglmCzA9T.jpg', '50000', 'wisata-jogja-1-hari', '2022-09-29 20:01:07', '2022-09-30 09:07:34'),
(2, 'Wisata Jogja 2 hari', 'Paket Wisata Jogja 2 hari', 'tour_categori/F4fPQC58qmALRn1g3WXxQ0abXI9x6lJ5v1eYE287.jpg', '50000', 'wisata-jogja-2-hari', '2022-09-29 20:19:39', '2022-09-30 09:07:40'),
(3, 'Wisata Jogja 3 hari', 'Paket Wisata Jogja 3 hari', 'tour_categori/nD04TwFgL9Ch29LJSSZFJmzr9sI0PlBXcCDzurjC.jpg', '1000', 'wisata-jogja-3-hari', '2022-09-29 20:20:09', '2022-09-30 09:07:49'),
(4, 'Wisata Jogja 4 hari', 'Paket Wisata Jogja 4 hari', 'tour_categori/8p3WD708y4rRU3A8vYOVsDhzxSkooJicVdGCxqxC.jpg', '5000', 'wisata-jogja-4-hari', '2022-09-30 08:06:18', '2022-09-30 09:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `tour_price`
--

CREATE TABLE `tour_price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tour_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tour_price`
--

INSERT INTO `tour_price` (`id`, `price`, `jumlah`, `kategori`, `tour_id`, `created_at`, `updated_at`) VALUES
(26, '50000', '50', 'dewasa', 1, NULL, NULL),
(27, '60000', '50', 'dewasa', 1, NULL, NULL),
(28, '50', '50', 'dewasa', 2, NULL, NULL),
(29, '50', '50', 'dewasa', 3, NULL, NULL),
(30, '50', '50', 'dewasa', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `image`, `no_hp`, `token`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'yuda@gmail.com', 'admin', 'user/wS2eLC8MTJ7edxKSpZI3nGb8mS4GvJv9OXqfKJpP.png', '089674555439', 'OhWYeAQ7e8av0WWeBGagBd6QQuiwscj82XCREnoUe1cncA00R1CedfETeZ4f', 'yudafadilah248@gmail.com', NULL, '$2y$10$jucethCHrw8GjXEtJ80MQeIiwwPZUlSqUyERHiM1f6UFRSlAFjVBy', NULL, '2022-08-08 19:54:39', '2022-08-08 20:47:05'),
(2, 'Yuda', 'admin', 'user/46KdA87oXiELX0WYTPoyeaHJjuH1DcTNPALvEZMV.png', '089674555439', 'VXmGclBpWXHXFmzmFtHIKfUu9GmgLIagaKcluFz2bfYo1ST84h8A6qZr4KjV', 'yuda@gmail.com', NULL, '$2y$10$MYC4CBVotd9ahOcWyt9ZEeg84RvbucPIdHKbGrYcTYkKbTQgFhsyO', NULL, '2022-08-08 20:03:29', '2022-08-08 20:03:29'),
(3, 'yuda@gmail.com', 'admin', 'user/PIAsbWcHVRMUCh0cQ0Ls94C431MRJMQT4yx4H0wg.png', '089674555439', 'MsM59FmEZubfP4F2rdLs0D2XZVzxYhUj1rejl0XRrlaWV9XZPe8MZM1wnGoM', 'aaaaaaaaaaa@gmail.com', NULL, '$2y$10$v4SizICDEKKbu75e16aRH.tmV8vNIPwmTOU281BU.CCLt0Liiwy2O', NULL, '2022-08-08 20:13:35', '2022-08-08 20:37:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikels`
--
ALTER TABLE `artikels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bus`
--
ALTER TABLE `bus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboards`
--
ALTER TABLE `dashboards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonis`
--
ALTER TABLE `testimonis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_categories`
--
ALTER TABLE `tour_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_price`
--
ALTER TABLE `tour_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikels`
--
ALTER TABLE `artikels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bus`
--
ALTER TABLE `bus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dashboards`
--
ALTER TABLE `dashboards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonis`
--
ALTER TABLE `testimonis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tour_categories`
--
ALTER TABLE `tour_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tour_price`
--
ALTER TABLE `tour_price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
