<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('harga');
            $table->text('image');
            $table->integer('diskon');
            $table->string('all_in');
            $table->string('seat');
            $table->enum('type', ['HD', 'UHD', 'SHD', 'HDD', 'ELF', 'HIACE']);
            $table->enum('transmisi', ['manual', 'autmatic']);
            $table->enum('p3k', ['1', '0']);
            $table->enum('audio', ['1', '0']);
            $table->enum('ac', ['1', '0']);
            $table->enum('charger', ['1', '0']);
            $table->enum('status', ['1', '0']);
            $table->enum('bensin', ['pertalite', 'pertamax', 'solar']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus');
    }
}
