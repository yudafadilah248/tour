<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('harga');
            $table->text('image');
            $table->integer('diskon');
            $table->string('kunci_12');
            $table->string('kunci_full');
            $table->string('sopir_12');
            $table->string('sopir_full');
            $table->string('all_12');
            $table->string('all_full');
            $table->string('seat');
            $table->enum('type', ['hathback', 'city', 'mpv']);
            $table->enum('transmisi', ['manual', 'autmatic']);
            $table->enum('p3k', ['1', '0']);
            $table->enum('audio', ['1', '0']);
            $table->enum('ac', ['1', '0']);
            $table->enum('charger', ['1', '0']);
            $table->enum('status', ['1', '0']);
            $table->enum('bensin', ['pertalite', 'pertamax', 'solar']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
