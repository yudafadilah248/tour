<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profil', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('deskrips_singkat');
            $table->text('deskripsi');
            $table->string('kontak');
            $table->string('text_wa');
            $table->string('ig');
            $table->string('fb');
            $table->text('alamat');
            $table->string('maps');
            $table->text('visi');
            $table->text('misi');
            $table->string('email');
            $table->string('senin');
            $table->string('selasa');
            $table->string('rabu');
            $table->string('kamis');
            $table->string('jumat');
            $table->string('sabtu');
            $table->string('minggu');
            $table->text('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profil');
    }
}
