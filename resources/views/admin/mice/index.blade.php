@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Management Mice</h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.mice.update_mice') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @error('meeting') has-danger @enderror">
                                    <label class="control-label">Meeting</label>
                                    <textarea name="meeting" id="editor" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan meeting Text">{{ $mice->meeting }}</textarea>
                                    @error('meeting')
                                        <label id="meeting-error" class="error mt-2 text-danger"
                                            for="meeting">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('gathering') has-danger @enderror">
                                    <label class="control-label">Gathering</label>
                                    <textarea name="gathering" id="editor1" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan gathering Text">{{ $mice->gathering }}</textarea>
                                    @error('gathering')
                                        <label id="gathering-error" class="error mt-2 text-danger"
                                            for="gathering">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('event') has-danger @enderror">
                                    <label class="control-label">Event</label>
                                    <textarea name="event" id="editor2" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan event Text">{{ $mice->event }}</textarea>
                                    @error('event')
                                        <label id="event-error" class="error mt-2 text-danger"
                                            for="event">{{ $message }}</label>
                                    @enderror
                                </div>

                                <br>
                                <button type="submit" class="btn btn-primary submit">Simpan</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- row -->
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#editor1'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#editor2'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#editor3'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
