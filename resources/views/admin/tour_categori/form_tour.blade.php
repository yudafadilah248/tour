@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0"> Tour Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <form
                    action="{{ @$edit_mode ? route('admin.tour_categori.update_tour', $tour->id) : route('admin.tour_categori.store_tour', $categori->id) }}"
                    enctype="multipart/form-data" method="POST">
                    @csrf
                    @if (@$edit_mode)
                        @method('PUT')
                    @endif
                    <div class="card-header">
                        <div class="col-12 d-flex align-items-center">
                            <h4 class="card-title mb-0">
                                @if (@$edit_mode)
                                    Edit Tour - {{ $tour->name }}
                                @else
                                    Tambah Tour
                                @endif
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Nama</label>
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan nama"
                                        value="{{ @$edit_mode ? $tour->name : old('name') }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group @error('start_price') has-danger @enderror">
                                            <label class="control-label">Harga Mulai</label>
                                            <input type="number" name="start_price" class="form-control"
                                                placeholder="Masukkan harga mulai"
                                                value="{{ @$edit_mode ? $tour->start_price : old('start_price') }}">
                                            @error('start_price')
                                                <label id="start_price-error" class="error mt-2 text-danger"
                                                    for="start_price">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group @error('diskon') has-danger @enderror">
                                            <label class="control-label">diskon</label>
                                            <input type="number" name="diskon" class="form-control"
                                                placeholder="Masukkan Diskon"
                                                value="{{ @$edit_mode ? $tour->diskon : old('diskon') }}">
                                            @error('diskon')
                                                <label id="diskon-error" class="error mt-2 text-danger"
                                                    for="diskon">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ @$edit_mode ? asset('uploads/' . $tour->image) : '' }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('wisata') has-danger @enderror">
                                    <label class="control-label">Wisata</label>
                                    <textarea name="wisata" id="editor" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan wisata">{{ @$edit_mode ? $tour->wisata : old('wisata') }}</textarea>
                                    @error('wisata')
                                        <label id="wisata-error" class="error mt-2 text-danger"
                                            for="wisata">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('itenary') has-danger @enderror">
                                    <label class="control-label">Acara</label>
                                    <textarea name="itenary" id="editor1" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan itenary">{{ @$edit_mode ? $tour->itenary : old('itenary') }}</textarea>
                                    @error('itenary')
                                        <label id="itenary-error" class="error mt-2 text-danger"
                                            for="itenary">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            {{-- <div class="form-group" style="margin-top : 20px;">
                                <a href="{{ route('admin.tour_categori.index') }}"
                                    class="btn btn-secondary submit">Batal</a>
                                <button type="submit" class="btn btn-primary submit">Simpan</button>
                            </div> --}}
                        </div>
                    </div>
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-baseline md-2">
                            <h4 class="card-title mb-0">
                                Detail Harga
                            </h4>
                            @if (@$edit_mode)
                                <p><button type="button" name="add" id="add"
                                        class="btn btn-primary btn-sm mb-3">Tambah</button></p>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table table-bordered" id="dynamic_field">
                                <thead>
                                    <tr>
                                        {{-- <th>Kategori</th> --}}
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (@$edit_mode)
                                        @foreach ($tour_price as $item)
                                            <tr>
                                                {{-- <td>
                                                    <input type="text" name="kategori[]" class="form-control" readonly
                                                        value="{{ $item->kategori }}">
                                                </td> --}}
                                                <td>
                                                    <input type="text" name="jumlah[]" class="form-control" readonly
                                                        value="{{ $item->jumlah }}">
                                                </td>
                                                <td>
                                                    <input type="text" name="price[]" class="form-control" readonly
                                                        value="{{ $item->price }}">
                                                </td>
                                                <td align="right">
                                                    {{-- {{ route('admin.tour_categori.edit_price', $item->id) }} --}}
                                                    <a href="" class="btn btn-info btn-block mb-1"
                                                        id="editCompany" data-toggle="modal"
                                                        data-target='#practice_modal'
                                                        data-id_price="{{ $item->id }}">Edit</a>
                                                    <a href="{{ route('admin.tour_categori.destroy_price', $item->id) }}"
                                                        class="btn btn-danger btn-block" title='Delete'>Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        <div class="modal fade" tabindex="-1" id="practice_modal" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <form id="companydata">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Detail Harga</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="hidden" id="tour_id" name="tour_id"
                                                                value="">
                                                            <input type="hidden" id="id_tour_price" name="id_tour_price"
                                                                value="">
                                                            {{-- <div
                                                                class="form-group @error('kategori') has-danger @enderror">
                                                                <label class="control-label">Kategori</label>
                                                                <select name="kategori" id="kategori"
                                                                    class="form-control" required>
                                                                    <option value=" "> -- Kategori -- </option>
                                                                    <option value="dewasa">Dewasa</option>
                                                                    <option value="anak">Anak </option>
                                                                    <option value="balita">Balita</option>
                                                                </select>
                                                            </div> --}}
                                                            <div class="form-group @error('jumlah') has-danger @enderror">
                                                                <label class="control-label">Jumlah</label>
                                                                <input type="text" id="jumlah" name="jumlah"
                                                                    class="form-control" value="">
                                                                @error('jumlah')
                                                                    <label id="jumlah-error" class="error mt-2 text-danger"
                                                                        for="jumlah">{{ $message }}</label>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group @error('price') has-danger @enderror">
                                                                <label class="control-label">Harga</label>
                                                                <input type="text" id="price" name="price"
                                                                    class="form-control" value="">
                                                                @error('price')
                                                                    <label id="price-error" class="error mt-2 text-danger"
                                                                        for="price">{{ $message }}</label>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="submit" id="submit_price"
                                                                class="btn btn-primary">Save
                                                                changes</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    @else
                                        <tr>
                                            {{-- <td>
                                                <select name="kategori[]" class="form-control" required>
                                                    <option value=" "> -- Kategori -- </option>
                                                    <option value="dewasa">Dewasa</option>
                                                    <option value="anak">Anak</option>
                                                    <option value="balita">Balita</option>
                                                </select>
                                            </td> --}}
                                            <td>
                                                <input type="text" name="jumlah[]" class="form-control" required
                                                    placeholder="Contoh Min 2 - 3 Org"
                                                    value="{{ @$edit_mode ? $tour->jumlah : old('jumlah') }}">
                                            </td>
                                            <td>
                                                <input type="number" name="price[]" class="form-control" required
                                                    placeholder="Masukkan harga"
                                                    value="{{ @$edit_mode ? $tour->price : old('price') }}">
                                            </td>
                                            <td align="right">
                                                <button type="button" name="add" id="add"
                                                    class="btn btn-primary btn-sm btn-block mb-3">Tambah</button>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group" style="margin-top : 20px;">
                            @if (@$edit_mode)
                                <a href="{{ route('admin.tour_categori.edit', ['tour_categori' => $tour->categori_id]) }}"
                                    class="btn btn-secondary submit">Batal</a>
                            @else
                                <a href="{{ route('admin.tour_categori.edit', ['tour_categori' => $categori->id]) }}"
                                    class="btn btn-secondary submit">Batal</a>
                            @endif
                            <button type="submit" class="btn btn-primary submit">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection
@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('click', '#submit_price', function(event) {
                event.preventDefault()
                var id = $("#id_tour_price").val();
                // var kategori = $("#kategori").val();
                var price = $("#price").val();
                var jumlah = $("#jumlah").val();

                // console.log(id);

                $.ajax({
                    url: '../update_price/' + id,
                    type: "POST",
                    data: {
                        price: price,
                        // kategori: kategori,
                        jumlah: jumlah,
                    },
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);

                        $('#companydata').trigger("reset");
                        $('#practice_modal').modal('hide');
                        window.location.reload(true);
                    }
                });
            });

            $('body').on('click', '#editCompany', function(event) {

                event.preventDefault();
                var id = $(this).data('id_price');
                console.log(id)
                $.get('../edit_price/' + id, function(data) {
                    console.log(data.data)
                    $('#practice_modal').modal('show');
                    $('#id_tour_price').val(data.data.id);
                    $('#jumlah').val(data.data.jumlah);
                    $('#price').val(data.data.price);
                    // $('#kategori option[value="' + data.data.kategori + '"]').prop('selected',
                    //     true);
                })
            });

        });
        //ADD PRODUK SIZE
        $(document).ready(function() {
            var i = 1;

            $('#add').click(function() {
                i++;

                $('#dynamic_field').append('<tr id="row' + i + '">' +
                    // '<td>' +
                    // '<select name="kategori[]" class="form-control" required >' +
                    // '<option value=" "> -- Kategori -- </option>' +
                    // '<option value="dewasa">Dewasa</option>' +
                    // '<option value="anak">Anak</option>' +
                    // '<option value="balita">Balita</option>' +
                    // '</select>' +
                    // '</td>' +
                    '<td><input type="text" name="jumlah[]" class="form-control" required placeholder="Contoh Min 2 - 3 Org"> </td>' +
                    '<td><input type="number" name="price[]" class="form-control" required placeholder="Masukkan Harga"> </td>' +
                    '<td><button type="button" name="remove" id="' + i +
                    '" class="btn btn-danger btn-sm btn-block btn_remove">Hapus</button></td></tr>'
                );
            });

            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

        });
    </script>
    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor1'))
            .catch(error => {
                console.error(error);
            });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: 'Anda yakin mau mengahapus data Detail Harga "' + name + '" ?',
                    dangerMode: true,
                    buttons: {
                        confirm: {
                            text: 'Yes'
                        },
                        cancel: 'Cancel'

                    },
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection
