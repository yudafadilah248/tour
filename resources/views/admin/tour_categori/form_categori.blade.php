@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Paket Wisata Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit Kategori wisata - {{ $categori->name }}
                            @else
                                Tambah Kategori wisata
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form
                        action="{{ @$edit_mode ? route('admin.tour_categori.update', ['tour_categori' => $categori->id]) : route('admin.tour_categori.store') }}"
                        enctype="multipart/form-data" method="POST">
                        @csrf
                        @if (@$edit_mode)
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Nama</label>
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan nama"
                                        value="{{ @$edit_mode ? $categori->name : old('name') }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('deskripsi') has-danger @enderror">
                                    <label class="control-label">Deskripsi</label>
                                    <input type="text" name="deskripsi" class="form-control" placeholder="Masukkan kode"
                                        value="{{ @$edit_mode ? $categori->deskripsi : old('deskripsi') }}">
                                    @error('deskripsi')
                                        <label id="deskripsi-error" class="error mt-2 text-danger"
                                            for="deskripsi">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('price') has-danger @enderror">
                                    <label class="control-label">Harga Mulai</label>
                                    <input type="text" name="price" class="form-control"
                                        placeholder="Masukkan harga mulai"
                                        value="{{ @$edit_mode ? $categori->price : old('price') }}">
                                    @error('price')
                                        <label id="price-error" class="error mt-2 text-danger"
                                            for="price">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ @$edit_mode ? asset('uploads/' . $categori->image) : '' }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div>
                                <a href="{{ route('admin.tour_categori.index') }}"
                                    class="btn btn-secondary submit">Batal</a>
                                <button type="submit" class="btn btn-primary submit">Simpan</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    @if (@$edit_mode)
        <div class="row">
            <div class="col-md-12 grid-margin">
                <h4>Manajemen Data Tour</h4>
                <hr>
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline md-2">
                            <h6 class="card-title">Data Tour</h6>
                            <p><a href="{{ route('admin.tour_categori.create_tour', $categori->id) }}"
                                    class="btn btn-primary ">Tambah
                                    Tour</a></p>
                        </div>
                        <div class="table-responsive">
                            <table id="dataTableExample" class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th>Harga Mulai</th>
                                        <th>Diskon</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1 @endphp
                                    @foreach ($tour as $item)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td><img src="{{ asset('uploads/' . $item->image) }}" alt=""
                                                    class="img-fluid" width="100px"></td>
                                            <td>Rp. {{ number_format($item->start_price, 0, ',', '.') }}</td>
                                            <td>{{ $item->diskon }}%</td>
                                            <td align="right">

                                                <a href="{{ route('admin.tour_categori.edit_tour', $item->id) }}"
                                                    class="btn btn-info">Edit</a>

                                                <form action="{{ route('admin.tour_categori.destroy_tour', $item->id) }}"
                                                    method="POST" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger show_confirm"
                                                        title='Delete' data-name="{{ $item->name }}">Delete</button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('include-css')
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
@endsection
@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: 'Anda yakin mau mengahapus data Tour "' + name + '" ?',
                    dangerMode: true,
                    buttons: {
                        confirm: {
                            text: 'Yes'
                        },
                        cancel: 'Cancel'

                    },
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection
