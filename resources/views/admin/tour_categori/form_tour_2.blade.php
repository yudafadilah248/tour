@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0"> Tour Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit Tour - {{ $tour->name }}
                            @else
                                Tambah Tour
                            @endif
                        </h4>
                    </div>
                </div>
                <form
                    action="{{ @$edit_mode ? route('admin.tour_categori.update_tour', $tour->id) : route('admin.tour_categori.store_tour', $categori->id) }}"
                    enctype="multipart/form-data" method="POST">
                    @csrf
                    @if (@$edit_mode)
                        @method('PUT')
                    @endif
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan name "
                                        value="{{ @$edit_mode ? $tour->name : old('name') }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @error('start_price') has-danger @enderror">
                                    <label class="control-label">Harga Mulai</label>
                                    <input type="text" name="start_price" class="form-control"
                                        placeholder="Masukkan harga mulai"
                                        value="{{ @$edit_mode ? $tour->start_price : old('start_price') }}">
                                    @error('start_price')
                                        <label id="start_price-error" class="error mt-2 text-danger"
                                            for="start_price">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-header">
                        <div class="col-12 d-flex align-items-center">
                            <h4 class="card-title mb-0">
                                Lokasi
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table table-bordered" id="DynamicLokasi">
                                <thead>
                                    <tr>
                                        <th>lokasi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="text" name="wisata[]" class="form-control" required
                                                placeholder="Masukkan nama wisata"
                                                value="{{ @$edit_mode ? $tour->wisata : old('wisata') }}">
                                        </td>
                                        <td align="right">
                                            <button type="button" name="add_lokasi" id="add_lokasi"
                                                class="btn btn-primary btn-sm btn-block mb-3">Tambah</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-header">
                        <div class="col-12 d-flex align-items-center">
                            <h4 class="card-title mb-0">
                                Detail Harga
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table table-bordered" id="dynamic_field">
                                <thead>
                                    <tr>
                                        <th>Kategori</th>
                                        <th>code</th>
                                        <th>price</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select name="kategori[]" class="form-control" required>
                                                <option value=" "> -- Kategori -- </option>
                                                @if (@$edit_mode)
                                                    <option
                                                        value="dewasa"{{ $tour->kategori == 'dewasa' ? 'selected' : '' }}>
                                                        Dewasa
                                                    </option>
                                                    <option value="anak"{{ $tour->kategori == 'anak' ? 'selected' : '' }}>
                                                        Anak
                                                    </option>
                                                    <option
                                                        value="balita"{{ $tour->kategori == 'balita' ? 'selected' : '' }}>
                                                        Balita
                                                    </option>
                                                @else
                                                    <option value="dewasa">Dewasa</option>
                                                    <option value="anak">Anak</option>
                                                    <option value="balita">Balita</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="jumlah[]" class="form-control" required
                                                placeholder="Contoh Min 2 - 3 Org"
                                                value="{{ @$edit_mode ? $tour->jumlah : old('jumlah') }}">
                                        </td>
                                        <td>
                                            <input type="text" name="price[]" class="form-control" required
                                                placeholder="Masukkan harga"
                                                value="{{ @$edit_mode ? $tour->price : old('price') }}">
                                        </td>
                                        <td align="right">
                                            <button type="button" name="add" id="add"
                                                class="btn btn-primary btn-sm btn-block mb-3">Tambah</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-header">
                        <div class="col-12 d-flex align-items-center">
                            <h4 class="card-title mb-0">
                                Lokasi
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table class="table table-bordered" id="DynamicAcara">
                                <thead>
                                    <tr>
                                        <th>lokasi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="text" name="wisata[]" class="form-control" required
                                                placeholder="Masukkan nama wisata"
                                                value="{{ @$edit_mode ? $tour->wisata : old('wisata') }}">
                                        </td>
                                        <td align="right">
                                            <button type="button" name="add_acara" id="add_acara"
                                                class="btn btn-primary btn-sm btn-block mb-3">Tambah</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group" style="margin-top : 20px;">
                            <a href="{{ route('admin.tour_categori.index') }}" class="btn btn-secondary submit">Batal</a>
                            <button type="submit" class="btn btn-primary submit">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('include-css')
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection
@section('include-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: 'Anda yakin mau mengahapus data Font "' + name + '" ?',
                    dangerMode: true,
                    buttons: {
                        confirm: {
                            text: 'Yes'
                        },
                        cancel: 'Cancel'

                    },
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        //ADD PRODUK SIZE
        $(document).ready(function() {
            var y = 1;

            $('#add_acara').click(function() {
                y++;

                $('#DynamicAcara').append('<tr id="row' + y + '">' +
                    '<td><input type="text" name="itenary[]" class="form-control" required placeholder="Masukkan nama acara"> </td>' +
                    '<td><button type="button" name="remove" id="' + y +
                    '" class="btn btn-danger btn-sm btn-block btn_remove">Hapus</button></td></tr>'
                );
            });

            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

        });
    </script>
    <script>
        //ADD PRODUK SIZE
        $(document).ready(function() {
            var x = 1;

            $('#add_lokasi').click(function() {
                x++;

                $('#DynamicLokasi').append('<tr id="row' + x + '">' +
                    '<td><input type="text" name="wisata[]" class="form-control" required placeholder="Masukkan nama wisata"> </td>' +
                    '<td><button type="button" name="remove" id="' + x +
                    '" class="btn btn-danger btn-sm btn-block btn_remove">Hapus</button></td></tr>'
                );
            });

            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

        });
    </script>
    <script>
        //ADD PRODUK SIZE
        $(document).ready(function() {
            var i = 1;

            $('#add').click(function() {
                i++;

                $('#dynamic_field').append('<tr id="row' + i + '">' +
                    '<td>' +
                    '<select name="kategori[]" class="form-control" required >' +
                    '<option value=" "> -- Kategori -- </option>' +
                    '<option value="dewasa">Dewasa</option>' +
                    '<option value="anak">Anak</option>' +
                    '<option value="balita">Balita</option>' +
                    '</select>' +
                    '</td>' +
                    '<td><input type="text" name="jumlah[]" class="form-control" required placeholder="Contoh Min 2 - 3 Org"> </td>' +
                    '<td><input type="text" name="price[]" class="form-control" required placeholder="Masukkan Harga"> </td>' +
                    '<td><button type="button" name="remove" id="' + i +
                    '" class="btn btn-danger btn-sm btn-block btn_remove">Hapus</button></td></tr>'
                );
            });

            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

        });
    </script>
@endsection
