@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">User Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit user - {{ $user->name }}
                            @else
                                Tambah user
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <form
                                action="{{ @$edit_mode ? route('admin.user.update', ['user' => $user->id]) : route('admin.user.store') }}"
                                enctype="multipart/form-data" method="POST">
                                @csrf
                                @if (@$edit_mode)
                                    @method('PUT')
                                @endif
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Nama</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter Nama "
                                        value="{{ @$edit_mode ? $user->name : old('name') }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('email') has-danger @enderror">
                                    <label class="control-label">Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="Enter Nama "
                                        value="{{ @$edit_mode ? $user->email : old('email') }}">
                                    @error('email')
                                        <label id="email-error" class="error mt-2 text-danger"
                                            for="email">{{ $message }}</label>
                                    @enderror
                                </div>
                                {{-- <div class="form-group @error('email') has-danger @enderror">
                                    <label class="control-label">Role</label>
                                    <select name="role" class="form-control">
                                        <option value=" "> -- role -- </option>
                                        <option value="admin"{{ @$edit_mode ? $user->role == 'admin' ? 'selected' : '' }}>admin</option>
                                    </select>
                                    @error('role')
                                        <label id="role-error" class="error mt-2 text-danger"
                                            for="role">{{ $message }}</label>
                                    @enderror
                                </div> --}}
                                <div class="form-group @error('no_hp') has-danger @enderror">
                                    <label class="control-label">No Hp</label>
                                    <input type="text" name="no_hp" class="form-control" placeholder="Enter Nama "
                                        value="{{ @$edit_mode ? $user->no_hp : old('no_hp') }}">
                                    @error('no_hp')
                                        <label id="no_hp-error" class="error mt-2 text-danger"
                                            for="no_hp">{{ $message }}</label>
                                    @enderror
                                </div>
                                @if (@$edit_password)
                                    <div class="form-group @error('password') has-danger @enderror">
                                        <label class="control-label">Password</label>
                                        <input type="password" name="password" class="form-control"
                                            placeholder="Enter Nama "
                                            value="{{ @$edit_mode ? $user->password : old('password') }}">
                                        @error('password')
                                            <label id="password-error" class="error mt-2 text-danger"
                                                for="password">{{ $message }}</label>
                                        @enderror
                                    </div>
                                @endif
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ @$edit_mode ? asset('uploads/' . $user->image) : '' }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div><br />
                                <a href="{{ route('admin.user.index') }}" class="btn btn-secondary submit">Batal</a>
                                <button type="submit" class="btn btn-primary submit">Simpan</button>
                            </form>
                        </div>
                        @if (@$edit_mode)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-12 d-flex align-items-center">
                                            <h4 class="card-title mb-0">Reset Password</h4>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('admin.user.update-password', $user->id) }}">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group @error('new_password') has-danger @enderror">
                                                <label class="control-label">Password</label>
                                                <div class="input-group mb-3">
                                                    <input type="password" class="form-control" id="password"
                                                        name="new_password" placeholder="Masukkan password baru Anda">
                                                    <div class="input-group-append">
                                                        <button class="btn form-control btn-outline-secondary"
                                                            type="button" onclick="change_password()" id="button-addon2"><i
                                                                class="fa fa-eye" id="icons"></i></button>
                                                    </div>
                                                </div>
                                                @error('new_password')
                                                    <label id="new_password-error" class="error mt-2 text-danger"
                                                        for="new_password">{{ $message }}</label>
                                                @enderror
                                            </div>
                                            <div class="form-group @error('password_confirmation') has-danger @enderror">
                                                <label class="control-label">Password</label>
                                                <div class="input-group mb-3">
                                                    <input type="password" class="form-control" id="password-confirm"
                                                        name="password_confirmation"
                                                        placeholder="Masukkan confirmasi password Anda">
                                                </div>
                                                @error('password_confirmation')
                                                    <label id="password_confirmation-error" class="error mt-2 text-danger"
                                                        for="password_confirmation">{{ $message }}</label>
                                                @enderror
                                            </div>
                                            <div class="form-group mt-2 mb-0">
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div> <!-- row -->
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
@endsection
