@extends('admin.layouts.master')
@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Welcome to Dashboard</h4>
        </div>
    </div>
    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h6 class="card-title text-uppercase mb-2" style="font-size: 18px">
                                        Mobil
                                    </h6>
                                </div>
                                <div class="col-auto">
                                    <!-- Icon -->
                                    <span class="h2 link-icon" data-feather="truck"></span>
                                </div>
                            </div>
                            <h3 class="mb-2 mt-2">{{ $car }}</h3>
                            <div class="d-flex align-items-baseline">
                                @if ($car_date != 0)
                                    <p class="text-success">
                                        <span>+{{ $car_date }}/Bulan</span>
                                        <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                    </p>
                                @else
                                    <p class="text-danger">
                                        <span>+{{ $car_date }}/Bulan</span>
                                        <i data-feather="arrow-down" class="icon-sm mb-1"></i>
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 grid-margin stretch-card">

                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <!-- Title -->
                                            <h6 class="card-title text-uppercase mb-2" style="font-size: 18px">
                                                Kategori
                                            </h6>
                                        </div>
                                    </div>
                                    <h3 class="mb-2 mt-2">{{ $kategori }}</h3>
                                    <div class="d-flex align-items-baseline">
                                        @if ($kategori_date != 0)
                                            <p class="text-success">
                                                <span>+{{ $kategori_date }}/Bulan</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        @else
                                            <p class="text-danger">
                                                <span>+{{ $kategori_date }}/Bulan</span>
                                                <i data-feather="arrow-down" class="icon-sm mb-1"></i>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="row align-items-center">
                                        <div class="col">
                                            <!-- Title -->
                                            <h6 class="card-title text-uppercase mb-2" style="font-size: 18px">
                                                Tour
                                            </h6>
                                        </div>
                                        <div class="col-auto">
                                            <!-- Icon -->
                                            <span class="h2 link-icon" data-feather="compass"></span>
                                        </div>
                                        {{-- <div class="jumlah">
                                                {{ $c_user }}
                                            </div> --}}
                                    </div>
                                    <h3 class="mb-2 mt-2">{{ $tour }}</h3>
                                    <div class="d-flex align-items-baseline">
                                        @if ($tour_date != 0)
                                            <p class="text-success">
                                                <span>+{{ $tour_date }}/Bulan</span>
                                                <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                            </p>
                                        @else
                                            <p class="text-danger">
                                                <span>+{{ $tour_date }}/Bulan</span>
                                                <i data-feather="arrow-down" class="icon-sm mb-1"></i>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h6 class="card-title text-uppercase mb-2" style="font-size: 18px">
                                        Artikel
                                    </h6>
                                </div>
                                <div class="col-auto">
                                    <!-- Icon -->
                                    <span class="h2 link-icon" data-feather="layout"></span>
                                </div>
                            </div>
                            <h3 class="mb-2 mt-2">{{ $artikel }}</h3>
                            <div class="d-flex align-items-baseline">
                                @if ($artikel_date != 0)
                                    <p class="text-success">
                                        <span>+{{ $artikel_date }}/Bulan</span>
                                        <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                    </p>
                                @else
                                    <p class="text-danger">
                                        <span>+{{ $artikel_date }}/Bulan</span>
                                        <i data-feather="arrow-down" class="icon-sm mb-1"></i>
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- row -->

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline md-2">
                        <h6 class="card-title">Top Penyewaan Mobil</h6>
                    </div>
                    <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Harga</th>
                                    <th>Diskon</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($top_car as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td><img src="{{ asset('uploads/' . $item->image) }}" alt=""
                                                class="img-fluid" width="100px"></td>
                                        <td>Rp. {{ number_format($item->harga, 0, ',', '.') }}</td>
                                        <td>{{ $item->diskon }}%</td>
                                        <td>
                                            @if ($item->status != 0)
                                                <span class="badge badge-success"><b>Publish</b></span>
                                            @else
                                                <span class="badge badge-danger"><b>Pending</b></span>
                                            @endif
                                        </td>
                                        <td align="right">

                                            <a href="{{ route('admin.cars.edit', ['car' => $item->id]) }}"
                                                class="btn btn-info">Edit</a>

                                            <form action="{{ route('admin.cars.destroy', ['car' => $item->id]) }}"
                                                method="POST" class="d-inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger show_confirm" title='Delete'
                                                    data-name="{{ $item->name }}">Delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('include-css')
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection
@section('include-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: 'Anda yakin mau mengahapus data Font "' + name + '" ?',
                    dangerMode: true,
                    buttons: {
                        confirm: {
                            text: 'Yes'
                        },
                        cancel: 'Cancel'

                    },
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection
