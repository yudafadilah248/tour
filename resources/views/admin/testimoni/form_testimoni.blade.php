@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Testimoni Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit Testimoni - {{ $testimoni->name }}
                            @else
                                Tambah Testimoni
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form
                        action="{{ @$edit_mode ? route('admin.testimoni.update', ['testimoni' => $testimoni->id]) : route('admin.testimoni.store') }}"
                        enctype="multipart/form-data" method="POST">
                        @csrf
                        @if (@$edit_mode)
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Nama</label>
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan Nama"
                                        value="{{ @$edit_mode ? $testimoni->name : old('name') }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('deskripsi') has-danger @enderror">
                                    <label class="control-label">Deskripsi</label>
                                    <textarea name="deskripsi" id="" cols="30" rows="5" class="form-control"
                                        placeholder="Masukkan Deskripsi">{{ @$edit_mode ? $testimoni->deskripsi : old('deskripsi') }}</textarea>
                                    @error('deskripsi')
                                        <label id="deskripsi-error" class="error mt-2 text-danger"
                                            for="deskripsi">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ @$edit_mode ? asset('uploads/' . $testimoni->image) : '' }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div><br />
                                <a href="{{ route('admin.testimoni.index') }}" class="btn btn-secondary submit">Batal</a>
                                <button type="submit" class="btn btn-primary submit">Simpan</button>
                            </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
    </div> <!-- row -->
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
