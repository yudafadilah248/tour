@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Bus Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit Bus - {{ $bus->name }}
                            @else
                                Tambah Bus
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form
                        action="{{ @$edit_mode ? route('admin.mobil.update', ['mobil' => $bus->id]) : route('admin.mobil.store') }}"
                        enctype="multipart/form-data" method="POST">
                        @csrf
                        @if (@$edit_mode)
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Name</label>
                                    <input type="text" name="name" class="form-control"
                                        placeholder="Masukkan nama Bis"
                                        value="{{ @$edit_mode ? $bus->name : old('name') }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('harga') has-danger @enderror">
                                    <label class="control-label">Harga mulai dari</label>
                                    <input type="number" name="harga" class="form-control"
                                        placeholder="Masukkan harga mulai dari"
                                        value="{{ @$edit_mode ? $bus->harga : old('harga') }}">
                                    @error('harga')
                                        <label id="harga-error" class="error mt-2 text-danger"
                                            for="harga">{{ $message }}</label>
                                    @enderror
                                </div>
                                {{-- <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group @error('diskon') has-danger @enderror">
                                            <label class="control-label">Diskon</label>
                                            <input type="number" name="diskon" class="form-control"
                                                placeholder="Masukkan diskon"
                                                value="{{ @$edit_mode ? $bus->diskon : old('diskon') }}">
                                            @error('diskon')
                                                <label id="diskon-error" class="error mt-2 text-danger"
                                                    for="diskon">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ @$edit_mode ? asset('uploads/' . $bus->image) : '' }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select name="type" class="form-control @error('type') is-invalid @enderror">
                                                <option value=" "> -- type -- </option>
                                                @if (@$edit_mode)
                                                    <option value="HD"{{ $bus->type == 'HD' ? 'selected' : '' }}>
                                                        HD
                                                    </option>
                                                    <option value="UHD"{{ $bus->type == 'UHD' ? 'selected' : '' }}>UHD
                                                    </option>
                                                    <option value="SHD"{{ $bus->type == 'SHD' ? 'selected' : '' }}>SHD
                                                    </option>
                                                    <option value="ELF"{{ $bus->type == 'ELF' ? 'selected' : '' }}>ELF
                                                    </option>
                                                    <option value="HIACE"{{ $bus->type == 'HIACE' ? 'selected' : '' }}>
                                                        HIACE
                                                    </option>
                                                @else
                                                    <option value="HD">HD</option>
                                                    <option value="UHD">UHD</option>
                                                    <option value="SHD">SHD</option>
                                                    <option value="ELF">ELF</option>
                                                    <option value="HIACE">HIACE</option>
                                                @endif
                                            </select>
                                            @error('type')
                                                <label id="type-error" class="text-danger pl-3"
                                                    for="type">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Audio</label>
                                            <select name="audio"
                                                class="form-control @error('audio') is-invalid @enderror">
                                                <option value=" "> -- audio -- </option>
                                                @if (@$edit_mode)
                                                    <option value="1"{{ $bus->audio == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                    <option value="0"{{ $bus->audio == '0' ? 'selected' : '' }}>Tidak
                                                    </option>
                                                @else
                                                    <option value="1">Iya</option>
                                                    <option value="0">Tidak</option>
                                                @endif
                                            </select>
                                            @error('audio')
                                                <label id="audio-error" class="text-danger pl-3"
                                                    for="audio">{{ $message }}</label>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Transmisi</label>
                                            <select name="transmisi"
                                                class="form-control @error('transmisi') is-invalid @enderror">
                                                <option value=" "> -- transmisi -- </option>
                                                @if (@$edit_mode)
                                                    <option
                                                        value="manual"{{ $bus->transmisi == 'manual' ? 'selected' : '' }}>
                                                        Manual
                                                    </option>
                                                    <option
                                                        value="autmatic"{{ $bus->transmisi == 'autmatic' ? 'selected' : '' }}>
                                                        Autmatic
                                                    </option>
                                                @else
                                                    <option value="manual">Manual</option>
                                                    <option value="autmatic">Autmatic</option>
                                                @endif
                                            </select>
                                            @error('transmisi')
                                                <label id="transmisi-error" class="text-danger pl-3"
                                                    for="transmisi">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>P3K</label>
                                            <select name="p3k" class="form-control @error('p3k') is-invalid @enderror">
                                                <option value=" "> -- P3k -- </option>
                                                @if (@$edit_mode)
                                                    <option value="1"{{ $bus->p3k == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                    <option value="0"{{ $bus->p3k == '0' ? 'selected' : '' }}>Tidak
                                                    </option>
                                                @else
                                                    <option value="1">Iya</option>
                                                    <option value="0">Tidak</option>
                                                @endif
                                            </select>
                                            @error('p3k')
                                                <label id="p3k-error" class="text-danger pl-3"
                                                    for="p3k">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Bensin</label>
                                            <select name="bensin"
                                                class="form-control @error('bensin') is-invalid @enderror">
                                                <option value=" "> -- bensin -- </option>
                                                @if (@$edit_mode)
                                                    <option
                                                        value="pertalite"{{ $bus->bensin == 'pertalite' ? 'selected' : '' }}>
                                                        Pertalite
                                                    </option>
                                                    <option
                                                        value="pertamax"{{ $bus->bensin == 'pertamax' ? 'selected' : '' }}>
                                                        Pertamax
                                                    </option>
                                                    <option value="solar"{{ $bus->bensin == 'solar' ? 'selected' : '' }}>
                                                        Solar
                                                    </option>
                                                @else
                                                    <option value="pertalite">Pertalite</option>
                                                    <option value="pertamax">Pertamax</option>
                                                    <option value="solar">Solar</option>
                                                @endif
                                            </select>
                                            @error('bensin')
                                                <label id="bensin-error" class="text-danger pl-3"
                                                    for="bensin">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>AC</label>
                                            <select name="ac" class="form-control @error('ac') is-invalid @enderror">
                                                <option value=" "> -- ac -- </option>
                                                @if (@$edit_mode)
                                                    <option value="1"{{ $bus->ac == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                    <option value="0"{{ $bus->ac == '0' ? 'selected' : '' }}>Tidak
                                                    </option>
                                                @else
                                                    <option value="1">Iya</option>
                                                    <option value="0">Tidak</option>
                                                @endif
                                            </select>
                                            @error('ac')
                                                <label id="ac-error" class="text-danger pl-3"
                                                    for="ac">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group @error('seat') has-danger @enderror">
                                            <label class="control-label">Seat</label>
                                            <input type="number" name="seat" class="form-control"
                                                placeholder="Masukkan jumlah kursi "
                                                value="{{ @$edit_mode ? $bus->seat : old('seat') }}">
                                            @error('seat')
                                                <label id="seat-error" class="error mt-2 text-danger"
                                                    for="seat">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Charger</label>
                                            <select name="charger"
                                                class="form-control @error('charger') is-invalid @enderror">
                                                <option value=" "> -- charger -- </option>
                                                @if (@$edit_mode)
                                                    <option value="1"{{ $bus->charger == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                    <option value="0"{{ $bus->charger == '0' ? 'selected' : '' }}>
                                                        Tidak
                                                    </option>
                                                @else
                                                    <option value="1">Iya</option>
                                                    <option value="0">Tidak</option>
                                                @endif
                                            </select>
                                            @error('charger')
                                                <label id="charger-error" class="text-danger pl-3"
                                                    for="charger">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Status Top Penyewaan Bus Kami</label>
                                            <select name="status"
                                                class="form-control @error('status') is-invalid @enderror">
                                                <option value=" "> -- Top Penyewaan Bus Kami -- </option>
                                                @if (@$edit_mode)
                                                    <option value="1"{{ $bus->status == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                    <option value="0"{{ $bus->status == '0' ? 'selected' : '' }}>
                                                        Tidak
                                                    </option>
                                                @else
                                                    <option value="1">Iya</option>
                                                    <option value="0">Tidak</option>
                                                @endif
                                            </select>
                                            @error('status')
                                                <label id="status-error" class="text-danger pl-3"
                                                    for="status">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @error('all_in') has-danger @enderror">
                                    <label class="control-label">Harga Bus All In</label>
                                    <input type="number" name="all_in" class="form-control"
                                        placeholder="Masukkan Harga Bus All In"
                                        value="{{ @$edit_mode ? $bus->all_in : old('all_in') }}">
                                    @error('all_in')
                                        <label id="all_in-error" class="error mt-2 text-danger"
                                            for="all_in">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <br />
                        <a href="{{ route('admin.mobil.index') }}" class="btn btn-secondary submit">Batal</a>
                        <button type="submit" class="btn btn-primary submit">Simpan</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
