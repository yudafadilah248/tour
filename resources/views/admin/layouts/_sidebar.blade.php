<nav class="sidebar">
    <div class="sidebar-header">
        <a href="#" class="sidebar-brand">
            Noble<span>UI</span>
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">
            <li class="nav-item {{ Route::is('admin.dashboard.*') ? 'active' : '' }}">
                <a href="{{ route('admin.dashboard.index') }}" class="nav-link">
                    <i class="link-icon" data-feather="codesandbox"></i>
                    <span class="link-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('admin.user.*') ? 'active' : '' }}">
                <a href="{{ route('admin.user.index') }}" class="nav-link">
                    <i class="link-icon" data-feather="users"></i>
                    <span class="link-title">User</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('admin.profil.*') ? 'active' : '' }}">
                <a href="{{ route('admin.profil.index') }}" class="nav-link">
                    <i class="link-icon" data-feather="info"></i>
                    <span class="link-title">Profil</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('admin.tour_categori.*') ? 'active' : '' }}">
                <a href="{{ route('admin.tour_categori.index') }}" class="nav-link">
                    <i class="link-icon" data-feather="compass"></i>
                    <span class="link-title">Wisata</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('admin.mice.*') ? 'active' : '' }}">
                <a href="{{ route('admin.mice.index') }}" class="nav-link">
                    <i class="link-icon" data-feather="calendar"></i>
                    <span class="link-title">Mice</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#authPages" role="button" aria-expanded="false"
                    aria-controls="authPages">
                    <i class="link-icon" data-feather="truck"></i>
                    <span class="link-title">Trans</span>
                    <i class="link-arrow" data-feather="chevron-down"></i>
                </a>
                <div class="collapse" id="authPages">
                    <ul class="nav sub-menu">
                        <li class="nav-item {{ Route::is('admin.cars.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.cars.index') }}" class="nav-link">
                                Mobil
                            </a>
                        </li>
                        <li class="nav-item {{ Route::is('admin.mobil.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.mobil.index') }}" class="nav-link">
                                Bus
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#authPages1" role="button" aria-expanded="false"
                    aria-controls="authPages1">
                    <i class="link-icon" data-feather="layout"></i>
                    <span class="link-title">Kontent</span>
                    <i class="link-arrow" data-feather="chevron-down"></i>
                </a>
                <div class="collapse" id="authPages1">
                    <ul class="nav sub-menu">
                        <li class="nav-item {{ Route::is('admin.galeri.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.galeri.index') }}" class="nav-link">
                                {{-- <i class="link-icon" data-feather="image"></i> --}}
                                Galeri
                            </a>
                        </li>
                        <li class="nav-item {{ Route::is('admin.artikel.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.artikel.index') }}" class="nav-link">
                                {{-- <i class="link-icon" data-feather="layout"></i> --}}
                                Artikel
                            </a>
                        </li>
                        <li class="nav-item {{ Route::is('admin.condition.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.condition.index') }}" class="nav-link">
                                {{-- <i class="link-icon" data-feather="list"></i> --}}
                                Persyaratan
                            </a>
                        </li>
                        <li class="nav-item {{ Route::is('admin.testimoni.*') ? 'active' : '' }}">
                            <a href="{{ route('admin.testimoni.index') }}" class="nav-link">
                                {{-- <i class="link-icon" data-feather="smile"></i> --}}
                                Testimoni
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            {{-- <li class="nav-item {{ Route::is('admin.cars.*') ? 'active' : '' }}">
                <a href="{{ route('admin.cars.index') }}" class="nav-link">
                    <i class="link-icon" data-feather="truck"></i>
                    <span class="link-title">Mobil</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('admin.mobil.*') ? 'active' : '' }}">
                <a href="{{ route('admin.mobil.index') }}" class="nav-link">
                    <i class="link-icon" data-feather="truck"></i>
                    <span class="link-title">Bus</span>
                </a>
            </li> --}}
            <li class="nav-item">
                <a href="{{ asset('assets/manual_book.pdf') }}" class="nav-link">
                    <i class="link-icon" data-feather="file-text"></i>
                    <span class="link-title">Manual Book</span>
                </a>
            </li>

        </ul>
    </div>
</nav>
