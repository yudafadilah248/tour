<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ @$page_title ? $page_title . ' - ' . config('app.name') : config('app.name') }}</title>
    <!-- core:css -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
    <link rel="stylesheet" href="{{ asset('vendors/core/core.css') }}">
    <!-- endinject -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('fonts/feather-font/css/iconfont.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/fullcalendar/main.min.css') }}">

    <!-- endinject -->
    <!-- Layout styles -->
    <!-- End layout styles -->

    @yield('dt')
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/bs4/dt-1.11.3/af-2.3.7/b-2.0.1/date-1.1.1/sb-1.2.2/datatables.min.css" />
    {{-- style data table --}}
    <!-- endinject -->
    @yield('include-css')
    <link rel="stylesheet" href="{{ asset('css/demo_1/style.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Layout styles -->
</head>

<body class="sidebar-dark">
    <div class="main-wrapper">
        <!-- partial:partials/_sidebar.html -->
        @include('admin.layouts._sidebar')
        <!-- partial -->

        <div class="page-wrapper">

            <!-- partial:partials/_navbar.html -->
            @include('admin.layouts._navbar')

            <!-- partial -->
            <div class="page-content">
                @yield('page_content')
            </div>

            <!-- partial:partials/_footer.html -->
            @include('admin.layouts._footer')
            <!-- partial -->

        </div>
    </div>

    @yield('modals')

    <!-- core:js -->
    <script src="{{ asset('vendors/core/core.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- endinject -->
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('vendors/fullcalendar/main.min.js') }}"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{ asset('vendors/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('js/template.js') }}"></script>
    <script src="{{ asset('js/datepicker.js') }}"></script>
    <!-- endinject -->
    <script src="{{ asset('js/data-table.js') }}"></script>
    @yield('include-js')
    <script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/dt-1.11.3/af-2.3.7/b-2.0.1/date-1.1.1/sb-1.2.2/datatables.min.js"></script>
    <!-- end custom js for this page -->

    <!-- ini js data table -->
    @yield('dt')
    <!-- endinject -->
</body>

</html>
