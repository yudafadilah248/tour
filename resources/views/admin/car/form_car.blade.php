@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Mobil Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit Mobil - {{ $car->name }}
                            @else
                                Tambah Mobil
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form
                        action="{{ @$edit_mode ? route('admin.cars.update', ['car' => $car->id]) : route('admin.cars.store') }}"
                        enctype="multipart/form-data" method="POST">
                        @csrf
                        @if (@$edit_mode)
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Nama Mobil</label>
                                    <input type="text" name="name" class="form-control"
                                        placeholder="Masukkan nama Mobil"
                                        value="{{ @$edit_mode ? $car->name : old('name') }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('harga') has-danger @enderror">
                                    <label class="control-label">Harga mulai dari</label>
                                    <input type="text" name="harga" class="form-control"
                                        placeholder="Masukkan harga mulai dari"
                                        value="{{ @$edit_mode ? $car->harga : old('harga') }}">
                                    @error('harga')
                                        <label id="harga-error" class="error mt-2 text-danger"
                                            for="harga">{{ $message }}</label>
                                    @enderror
                                </div>
                                {{-- <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group @error('diskon') has-danger @enderror">
                                            <label class="control-label">Diskon</label>
                                            <input type="text" name="diskon" class="form-control"
                                                placeholder="Masukkan diskon "
                                                value="{{ @$edit_mode ? $car->diskon : old('diskon') }}">
                                            @error('diskon')
                                                <label id="diskon-error" class="error mt-2 text-danger"
                                                    for="diskon">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ @$edit_mode ? asset('uploads/' . $car->image) : '' }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select name="type" class="form-control @error('type') is-invalid @enderror">
                                                <option value=" "> -- type -- </option>
                                                @if (@$edit_mode)
                                                    <option
                                                        value="hathback"{{ $car->type == 'hathback' ? 'selected' : '' }}>
                                                        Hathback
                                                    </option>
                                                    <option value="city"{{ $car->type == 'city' ? 'selected' : '' }}>City
                                                    </option>
                                                    <option value="mpv"{{ $car->type == 'mpv' ? 'selected' : '' }}>Mpv
                                                    </option>
                                                @else
                                                    <option value="hathback">Hathback</option>
                                                    <option value="city">city</option>
                                                    <option value="mpv">Mpv</option>
                                                @endif
                                            </select>
                                            @error('type')
                                                <label id="type-error" class="text-danger pl-3"
                                                    for="type">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Audio</label>
                                            <select name="audio"
                                                class="form-control @error('audio') is-invalid @enderror">
                                                <option value=" "> -- audio -- </option>
                                                @if (@$edit_mode)
                                                    <option value="0"{{ $car->audio == '0' ? 'selected' : '' }}>Tidak
                                                    </option>
                                                    <option value="1"{{ $car->audio == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                @else
                                                    <option value="0">Tidak</option>
                                                    <option value="1">Iya</option>
                                                @endif
                                            </select>
                                            @error('audio')
                                                <label id="audio-error" class="text-danger pl-3"
                                                    for="audio">{{ $message }}</label>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Transmisi</label>
                                            <select name="transmisi"
                                                class="form-control @error('transmisi') is-invalid @enderror">
                                                <option value=" "> -- transmisi -- </option>
                                                @if (@$edit_mode)
                                                    <option
                                                        value="manual"{{ $car->transmisi == 'manual' ? 'selected' : '' }}>
                                                        Manual
                                                    </option>
                                                    <option
                                                        value="autmatic"{{ $car->transmisi == 'autmatic' ? 'selected' : '' }}>
                                                        Autmatic
                                                    </option>
                                                @else
                                                    <option value="manual">Manual</option>
                                                    <option value="autmatic">Autmatic</option>
                                                @endif
                                            </select>
                                            @error('transmisi')
                                                <label id="transmisi-error" class="text-danger pl-3"
                                                    for="transmisi">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>P3K</label>
                                            <select name="p3k" class="form-control @error('p3k') is-invalid @enderror">
                                                <option value=" "> -- P3k -- </option>
                                                @if (@$edit_mode)
                                                    <option value="0"{{ $car->p3k == '0' ? 'selected' : '' }}>Tidak
                                                    </option>
                                                    <option value="1"{{ $car->p3k == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                @else
                                                    <option value="0">Tidak</option>
                                                    <option value="1">Iya</option>
                                                @endif
                                            </select>
                                            @error('p3k')
                                                <label id="p3k-error" class="text-danger pl-3"
                                                    for="p3k">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Bensin</label>
                                            <select name="bensin"
                                                class="form-control @error('bensin') is-invalid @enderror">
                                                <option value=" "> -- bensin -- </option>
                                                @if (@$edit_mode)
                                                    <option
                                                        value="pertalite"{{ $car->bensin == 'pertalite' ? 'selected' : '' }}>
                                                        Pertalite
                                                    </option>
                                                    <option
                                                        value="pertamax"{{ $car->bensin == 'pertamax' ? 'selected' : '' }}>
                                                        Pertamax
                                                    </option>
                                                    <option value="solar"{{ $car->bensin == 'solar' ? 'selected' : '' }}>
                                                        Solar
                                                    </option>
                                                @else
                                                    <option value="pertalite">Pertalite</option>
                                                    <option value="pertamax">Pertamax</option>
                                                    <option value="solar">Solar</option>
                                                @endif
                                            </select>
                                            @error('bensin')
                                                <label id="bensin-error" class="text-danger pl-3"
                                                    for="bensin">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>AC</label>
                                            <select name="ac" class="form-control @error('ac') is-invalid @enderror">
                                                <option value=" "> -- ac -- </option>
                                                @if (@$edit_mode)
                                                    <option value="0"{{ $car->ac == '0' ? 'selected' : '' }}>Tidak
                                                    </option>
                                                    <option value="1"{{ $car->ac == '1' ? 'selected' : '' }}>Iya
                                                    </option>
                                                @else
                                                    <option value="0">Tidak</option>
                                                    <option value="1">Iya</option>
                                                @endif
                                            </select>
                                            @error('ac')
                                                <label id="ac-error" class="text-danger pl-3"
                                                    for="ac">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group @error('seat') has-danger @enderror">
                                            <label class="control-label">Seat</label>
                                            <input type="number" name="seat" class="form-control"
                                                placeholder="Masukkan jumlah kursi "
                                                value="{{ @$edit_mode ? $car->seat : old('seat') }}">
                                            @error('seat')
                                                <label id="seat-error" class="error mt-2 text-danger"
                                                    for="seat">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Charger</label>
                                            <select name="charger"
                                                class="form-control @error('charger') is-invalid @enderror">
                                                <option value=" "> -- charger -- </option>
                                                @if (@$edit_mode)
                                                    <option value="0"{{ $car->charger == '0' ? 'selected' : '' }}>
                                                        Tidak
                                                    </option>
                                                    <option value="1"{{ $car->charger == '1' ? 'selected' : '' }}>
                                                        Iya
                                                    </option>
                                                @else
                                                    <option value="0">Tidak</option>
                                                    <option value="1">Iya</option>
                                                @endif
                                            </select>
                                            @error('charger')
                                                <label id="charger-error" class="text-danger pl-3"
                                                    for="charger">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Status Top Penyewaan Mobil Kami</label>
                                            <select name="status"
                                                class="form-control @error('status') is-invalid @enderror">
                                                <option value=" "> -- Status Mobil -- </option>
                                                @if (@$edit_mode)
                                                    <option value="0"{{ $car->status == '0' ? 'selected' : '' }}>
                                                        Tidak
                                                    </option>
                                                    <option value="1"{{ $car->status == '1' ? 'selected' : '' }}>
                                                        Iya
                                                    </option>
                                                @else
                                                    <option value="0">Tidak</option>
                                                    <option value="1">Iya</option>
                                                @endif
                                            </select>
                                            @error('status')
                                                <label id="status-error" class="text-danger pl-3"
                                                    for="status">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5 style="text-align: center; margin-top: 10px; margin-bottom: -10px">List Harga Mobil</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group @error('kunci_12') has-danger @enderror">
                                    <label class="control-label">Lepas Kunci 12 Jam</label>
                                    <input type="number" name="kunci_12" class="form-control"
                                        placeholder="Masukkan Harga Lepas Kunci 12 Jam"
                                        value="{{ @$edit_mode ? $car->kunci_12 : old('kunci_12') }}">
                                    @error('kunci_12')
                                        <label id="kunci_12-error" class="error mt-2 text-danger"
                                            for="kunci_12">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('kunci_full') has-danger @enderror">
                                    <label class="control-label">Lepas Kunci Full Day</label>
                                    <input type="number" name="kunci_full" class="form-control"
                                        placeholder="Masukkan Harga Lepas Kunci Full "
                                        value="{{ @$edit_mode ? $car->kunci_full : old('kunci_full') }}">
                                    @error('kunci_full')
                                        <label id="kunci_full-error" class="error mt-2 text-danger"
                                            for="kunci_full">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group @error('sopir_12') has-danger @enderror">
                                    <label class="control-label">Dengan Sopir 12 Jam</label>
                                    <input type="number" name="sopir_12" class="form-control"
                                        placeholder="Masukkan Harga Dengan Sopir 12 Jam"
                                        value="{{ @$edit_mode ? $car->sopir_12 : old('sopir_12') }}">
                                    @error('sopir_12')
                                        <label id="sopir_12-error" class="error mt-2 text-danger"
                                            for="sopir_12">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('sopir_full') has-danger @enderror">
                                    <label class="control-label">Dengan Sopir Full Day</label>
                                    <input type="number" name="sopir_full" class="form-control"
                                        placeholder="Masukkan Harga Sopir Full Day "
                                        value="{{ @$edit_mode ? $car->sopir_full : old('sopir_full') }}">
                                    @error('sopir_full')
                                        <label id="sopir_full-error" class="error mt-2 text-danger"
                                            for="sopir_full">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group @error('all_12') has-danger @enderror">
                                    <label class="control-label">Sopir + BBM 12 Jam</label>
                                    <input type="number" name="all_12" class="form-control"
                                        placeholder="Masukkan Harga Sopir + BBM 12 Jam "
                                        value="{{ @$edit_mode ? $car->all_12 : old('all_12') }}">
                                    @error('all_12')
                                        <label id="all_12-error" class="error mt-2 text-danger"
                                            for="all_12">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('all_full') has-danger @enderror">
                                    <label class="control-label">Sopir + BBM Full Day</label>
                                    <input type="number" name="all_full" class="form-control"
                                        placeholder="Masukkan Harga Sopir + BBM Full Day "
                                        value="{{ @$edit_mode ? $car->all_full : old('all_full') }}">
                                    @error('all_full')
                                        <label id="all_full-error" class="error mt-2 text-danger"
                                            for="all_full">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <br />
                        <a href="{{ route('admin.cars.index') }}" class="btn btn-secondary submit">Batal</a>
                        <button type="submit" class="btn btn-primary submit">Simpan</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
