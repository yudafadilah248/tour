@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Condition Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit Condition
                            @else
                                Tambah Condition
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form
                        action="{{ @$edit_mode ? route('admin.condition.update', ['condition' => $condition->id]) : route('admin.condition.store') }}"
                        enctype="multipart/form-data" method="POST">
                        @csrf
                        @if (@$edit_mode)
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @error('domisili') has-danger @enderror">
                                    <label class="control-label">Domisili Jogja</label>
                                    <textarea name="domisili" id="editor" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan domisili Text">{{ @$edit_mode ? $condition->domisili : old('domisili') }}</textarea>
                                    @error('domisili')
                                        <label id="domisili-error" class="error mt-2 text-danger"
                                            for="domisili">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('luar_domisili') has-danger @enderror">
                                    <label class="control-label">Luar Domisili Jogja</label>
                                    <textarea name="luar_domisili" id="editor1" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan luar_domisili Text">{{ @$edit_mode ? $condition->luar_domisili : old('luar_domisili') }}</textarea>
                                    @error('luar_domisili')
                                        <label id="luar_domisili-error" class="error mt-2 text-danger"
                                            for="luar_domisili">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('mahasiswa') has-danger @enderror">
                                    <label class="control-label">Mahasiswa</label>
                                    <textarea name="mahasiswa" id="editor2" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan mahasiswa Text">{{ @$edit_mode ? $condition->mahasiswa : old('mahasiswa') }}</textarea>
                                    @error('mahasiswa')
                                        <label id="mahasiswa-error" class="error mt-2 text-danger"
                                            for="mahasiswa">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('foreigner') has-danger @enderror">
                                    <label class="control-label">Foreigner / Turis</label>
                                    <textarea name="foreigner" id="editor3" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan foreigner Text">{{ @$edit_mode ? $condition->foreigner : old('foreigner') }}</textarea>
                                    @error('foreigner')
                                        <label id="foreigner-error" class="error mt-2 text-danger"
                                            for="foreigner">{{ $message }}</label>
                                    @enderror
                                </div>

                                <br>
                                <a href="{{ route('admin.condition.index') }}" class="btn btn-secondary submit">Batal</a>
                                <button type="submit" class="btn btn-primary submit">Simpan</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- row -->
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#editor1'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#editor2'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#editor3'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
