@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Management Profil</h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.profil.update_profil') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @error('name') has-danger @enderror">
                                    <label class="control-label">Nama</label>
                                    <input type="text" name="name" class="form-control" value="{{ $profil->name }}">
                                    @error('name')
                                        <label id="name-error" class="error mt-2 text-danger"
                                            for="name">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('alamat') has-danger @enderror">
                                    <label class="control-label">Alamat</label>
                                    <textarea name="alamat" class="form-control" id="" cols="30" rows="5">{{ $profil->alamat }}</textarea>
                                    @error('alamat')
                                        <label id="alamat-error" class="error mt-2 text-danger"
                                            for="alamat">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('deskripsi_singkat') has-danger @enderror">
                                    <label class="control-label">Deskripsi Singkat</label>
                                    <textarea name="deskripsi_singkat" class="form-control" id="" cols="30" rows="5">{{ $profil->deskripsi_singkat }}</textarea>
                                    @error('deskripsi_singkat')
                                        <label id="deskripsi_singkat-error" class="error mt-2 text-danger"
                                            for="deskripsi_singkat">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('deskripsi') has-danger @enderror">
                                    <label class="control-label">Deskripsi</label>
                                    <textarea name="deskripsi" id="editor2" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan Deskripsi">{{ $profil->deskripsi }}</textarea>
                                    @error('deskripsi')
                                        <label id="deskripsi-error" class="error mt-2 text-danger"
                                            for="deskripsi">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @error('kontak') has-danger @enderror">
                                    <label class="control-label">Kontak</label>
                                    <input type="text" name="kontak" class="form-control" value="{{ $profil->kontak }}">
                                    @error('kontak')
                                        <label id="kontak-error" class="error mt-2 text-danger"
                                            for="kontak">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('text_wa') has-danger @enderror">
                                    <label class="control-label">Text Whataspp</label>
                                    <input type="text" name="text_wa" class="form-control"
                                        value="{{ $profil->text_wa }}">
                                    @error('text_wa')
                                        <label id="text_wa-error" class="error mt-2 text-danger"
                                            for="text_wa">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('email') has-danger @enderror">
                                    <label class="control-label">Email</label>
                                    <input type="text" name="email" class="form-control" value="{{ $profil->email }}">
                                    @error('email')
                                        <label id="email-error" class="error mt-2 text-danger"
                                            for="email">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group @error('ig') has-danger @enderror">
                                            <label class="control-label">Instagram</label>
                                            <input type="text" name="ig" class="form-control"
                                                value="{{ $profil->ig }}">
                                            @error('ig')
                                                <label id="ig-error" class="error mt-2 text-danger"
                                                    for="ig">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group @error('fb') has-danger @enderror">
                                            <label class="control-label">Facebook</label>
                                            <input type="text" name="fb" class="form-control"
                                                value="{{ $profil->fb }}">
                                            @error('fb')
                                                <label id="fb-error" class="error mt-2 text-danger"
                                                    for="fb">{{ $message }}</label>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group @error('maps') has-danger @enderror">
                                    <label class="control-label">Maps</label>
                                    <input type="text" name="maps" class="form-control"
                                        value="{{ $profil->maps }}">
                                    @error('maps')
                                        <label id="maps-error" class="error mt-2 text-danger"
                                            for="maps">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ asset('uploads/' . $profil->image) }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @error('visi') has-danger @enderror">
                                    <label class="control-label">Visi</label>
                                    <textarea name="visi" id="editor" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan visi Text">{{ $profil->visi }}</textarea>
                                    @error('visi')
                                        <label id="visi-error" class="error mt-2 text-danger"
                                            for="visi">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('misi') has-danger @enderror">
                                    <label class="control-label">Misi</label>
                                    <textarea name="misi" id="editor1" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan misi Text">{{ $profil->misi }}</textarea>
                                    @error('misi')
                                        <label id="misi-error" class="error mt-2 text-danger"
                                            for="misi">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <p style="font-size: 16px; text-align:center; "><b>Hari</b></p>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @error('senin') has-danger @enderror">
                                    <label class="control-label">Senin</label>
                                    <input type="text" name="senin" class="form-control"
                                        value="{{ $profil->senin }}">
                                    @error('senin')
                                        <label id="senin-error" class="error mt-2 text-danger"
                                            for="senin">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('selasa') has-danger @enderror">
                                    <label class="control-label">Selasa</label>
                                    <input type="text" name="selasa" class="form-control"
                                        value="{{ $profil->selasa }}">
                                    @error('selasa')
                                        <label id="selasa-error" class="error mt-2 text-danger"
                                            for="selasa">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('rabu') has-danger @enderror">
                                    <label class="control-label">Rabu</label>
                                    <input type="text" name="rabu" class="form-control"
                                        value="{{ $profil->rabu }}">
                                    @error('rabu')
                                        <label id="rabu-error" class="error mt-2 text-danger"
                                            for="rabu">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('kamis') has-danger @enderror">
                                    <label class="control-label">Kamis</label>
                                    <input type="text" name="kamis" class="form-control"
                                        value="{{ $profil->kamis }}">
                                    @error('kamis')
                                        <label id="kamis-error" class="error mt-2 text-danger"
                                            for="kamis">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @error('jumat') has-danger @enderror">
                                    <label class="control-label">Jumat</label>
                                    <input type="text" name="jumat" class="form-control"
                                        value="{{ $profil->jumat }}">
                                    @error('jumat')
                                        <label id="jumat-error" class="error mt-2 text-danger"
                                            for="jumat">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('sabtu') has-danger @enderror">
                                    <label class="control-label">Sabtu</label>
                                    <input type="text" name="sabtu" class="form-control"
                                        value="{{ $profil->sabtu }}">
                                    @error('sabtu')
                                        <label id="sabtu-error" class="error mt-2 text-danger"
                                            for="sabtu">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('minggu') has-danger @enderror">
                                    <label class="control-label">Minggu</label>
                                    <input type="text" name="minggu" class="form-control"
                                        value="{{ $profil->minggu }}">
                                    @error('minggu')
                                        <label id="minggu-error" class="error mt-2 text-danger"
                                            for="minggu">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary submit">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>

    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor1'))
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor2'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
