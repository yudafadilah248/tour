@extends('admin.layouts.master')
@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h5 class="mb-2 mb-md-0">Manajmen Artikel</h5>
        </div>
    </div>
    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-baseline md-2">
                        <h6 class="card-title">Data Artikel</h6>
                        <p><a href="{{ route('admin.artikel.create') }}" class="btn btn-primary ">Tambah Artikel</a></p>
                    </div>
                    <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Gambar</th>
                                    <th>Status</th>
                                    <th>Tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1 @endphp
                                @foreach ($artikels as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->judul }}</td>
                                        <td><img src="{{ asset('uploads/' . $item->image) }}" alt=""
                                                class="img-fluid" width="100px"></td>
                                        <td>
                                            @if ($item->status != 0)
                                                <span class="badge badge-success"><b>Publish</b></span>
                                            @else
                                                <span class="badge badge-danger"><b>Pending</b></span>
                                            @endif
                                        </td>
                                        <td>{{ $item->tanggal }}</td>
                                        <td align="right">

                                            <a href="{{ route('admin.artikel.edit', ['artikel' => $item->id]) }}"
                                                class="btn btn-info">Edit</a>

                                            <form action="{{ route('admin.artikel.destroy', ['artikel' => $item->id]) }}"
                                                method="POST" class="d-inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger show_confirm" title='Delete'
                                                    data-name="{{ $item->judul }}">Delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('include-css')
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection
@section('include-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                    title: 'Anda yakin mau mengahapus data Font "' + name + '" ?',
                    dangerMode: true,
                    buttons: {
                        confirm: {
                            text: 'Yes'
                        },
                        cancel: 'Cancel'

                    },
                })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
@endsection
