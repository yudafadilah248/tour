@extends('admin.layouts.master')

@section('page_content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Artikel Management </h4>
        </div>
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            {{--  --}}
        </div>
    </div>

    @include('admin.layouts.alert')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 d-flex align-items-center">
                        <h4 class="card-title mb-0">
                            @if (@$edit_mode)
                                Edit Artikel - {{ $artikel->name }}
                            @else
                                Tambah Artikel
                            @endif
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form
                        action="{{ @$edit_mode ? route('admin.artikel.update', ['artikel' => $artikel->id]) : route('admin.artikel.store') }}"
                        enctype="multipart/form-data" method="POST">
                        @csrf
                        @if (@$edit_mode)
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group @error('judul') has-danger @enderror">
                                    <label class="control-label">Judul</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Masukkan Judul"
                                        value="{{ @$edit_mode ? $artikel->judul : old('judul') }}">
                                    @error('judul')
                                        <label id="judul-error" class="error mt-2 text-danger"
                                            for="judul">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('kutipan') has-danger @enderror">
                                    <label class="control-label">Kutipan</label>
                                    <textarea name="kutipan" id="" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan Kutipan">{{ @$edit_mode ? $artikel->kutipan : old('kutipan') }}</textarea>
                                    @error('kutipan')
                                        <label id="kutipan-error" class="error mt-2 text-danger"
                                            for="kutipan">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group @error('artikel') has-danger @enderror">
                                    <label class="control-label">Artikel</label>
                                    <textarea name="artikel" id="editor" cols="30" rows="10" class="form-control"
                                        placeholder="Masukkan Artikel">{{ @$edit_mode ? $artikel->artikel : old('artikel') }}</textarea>
                                    @error('artikel')
                                        <label id="artikel-error" class="error mt-2 text-danger"
                                            for="artikel">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group @error('image') has-danger @enderror">
                                    <label>Image</label>
                                    <input type="file" class="use-dropify" name="image"
                                        data-default-file="{{ @$edit_mode ? asset('uploads/' . $artikel->image) : '' }}">
                                    @error('image')
                                        <label id="image-error" class="error mt-2 text-danger"
                                            for="image">{{ $message }}</label>
                                    @enderror
                                </div><br />
                                <div class="form-group @error('status') has-danger @enderror">
                                    <label class="control-label">Status</label>
                                    <select class="form-control" name="status" id="exampleFormControlSelect1">
                                        <option selected disabled>Pilih Status</option>
                                        @if (@$edit_mode)
                                            <option value="1" {{ $artikel->status == '1' ? 'selected' : '' }}>Publish
                                            </option>
                                            <option value="0 " {{ $artikel->status == '0' ? 'selected' : '' }}>Pending
                                            </option>
                                        @else
                                            <option value="1">Publish </option>
                                            <option value="0">Pending </option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group @error('tanggal') has-danger @enderror">
                                    <label class="control-label">Tanggal</label>
                                    <input type="date" class="form-control"
                                        value="{{ @$edit_mode ? $artikel->tanggal : old('tanggal') }}" name="tanggal"
                                        required>
                                    @error('tanggal')
                                        <label id="tanggal-error" class="error mt-2 text-danger"
                                            for="tanggal">{{ $message }}</label>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-md-12">
                                <a href="{{ route('admin.artikel.index') }}" class="btn btn-secondary submit">Batal</a>
                                <button type="submit" class="btn btn-primary submit">Simpan</button>
                            </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
    </div> <!-- row -->
@endsection

@section('include-css')
    <link rel="stylesheet" href="{{ asset('vendors/dropify/dist/dropify.min.css') }}">
    <style type="text/css">
        .swal-modal {
            width: 300px;
            height: auto;
        }

        .swal-title {
            font-size: 18px;
        }

        .swal-footer {
            text-align: center;
        }
    </style>
@endsection

@section('include-js')
    <script src="{{ asset('vendors/dropify/dist/dropify.min.js') }}"></script>
    <script>
        $(".use-dropify").dropify();
    </script>
    <script>
        $(function() {
            'use strict';

            $('#myDropify').dropify();
        });
    </script>
    <script script script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
