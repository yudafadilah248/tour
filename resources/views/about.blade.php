@section('include-css')
@endsection
@extends('layouts.master')
@section('page_content')
    <div class="banner pt">
        <div class="container">
            <p class="head-blog">Tentang Kami</p>
        </div>
    </div>

    <div class="container">
        <p class="asta">{{ $profil->name }}</p>
        <div class="row">
            <div class="col-md-4">
                <img src="{{ asset('assets/img/logo.png') }}" alt="" class="img-about">
            </div>
            <div class="col-md-8 desc-about">
                {!! $profil->deskripsi !!}
            </div>
        </div>

        <div class="accordion">
            <div class="accordion-item">
                <button id="accordion-button-1" aria-expanded="true"><span class="accordion-title">Visi Kami</span><span
                        class="icon" aria-hidden="true"></span></button>
                <div class="accordion-content">
                    {!! $profil->visi !!}
                </div>
            </div>
            <div class="accordion-item">
                <button id="accordion-button-2" aria-expanded="false"><span class="accordion-title">Misi
                        Kami</span><span class="icon" aria-hidden="true"></span></button>
                <div class="accordion-content">
                    {!! $profil->misi !!}
                </div>
            </div>
        </div>

    </div>

    {{-- TOP --}}
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-top">
                <p class="title-1">Top Penyewaan Mobil Kami</p>
                <p class="deskripsi-1">Astha Trans mengutamakan kenyaman para pelanggan dengan unit terbaru dan terawat,
                    ditambah dengan tenaga driver kami yang berpengalaman dan harga sewa yang terjangkau.</p>
                <hr class="garis-1">
            </div>
        </div>

        @foreach ($top_car as $key => $item)
            <div class="row-2">
                <div class="kotak">
                    @if ($item->diskon != 0)
                        <div class="diskon">{{ $item->diskon }}%</div>
                    @endif
                    <div class="col-md-6">
                        <p class="name-car">{{ $item->name }}</p>
                        <img src="{{ asset('uploads/' . $item->image) }}" class="image-car" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="posisi">
                            <div class="box-price">
                                Harga Mulai : Rp. {{ number_format($item->harga, 0, ',', '.') }} / Full Day
                            </div>
                            <div class="navbar justify-content-center">
                                <div class="nav">
                                    <button class="box-spek active" id="v-pills-home-{{ $key }}-tab"
                                        data-toggle="pill" data-target="#v-pills-home-{{ $key }}" role="tab"
                                        aria-controls="v-pills-home-{{ $key }}"
                                        aria-selected="true">Spesifikasi</button>
                                    <button class="box-detail" id="v-pills-profile-{{ $key }}-tab"
                                        data-toggle="pill" data-target="#v-pills-profile-{{ $key }}"
                                        role="tab" aria-controls="v-pills-profile-{{ $key }}"
                                        aria-selected="false">Detail
                                        Harga</button>
                                </div>
                            </div>
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active custom" id="v-pills-home-{{ $key }}"
                                    aria-labelledby="v-pills-home-{{ $key }}-tab">
                                    <table class="tabel-spek">
                                        <tr>
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-tint icon-spek" aria-hidden="true"
                                                            style="margin-left: 7px;"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->bensin == 'pertalite')
                                                        Pertalite
                                                    @elseif ($item->bensin == 'pertamax')
                                                        Pertamax
                                                    @else
                                                        Solar
                                                    @endif
                                                </p>
                                            </td>
                                            @if ($item->charger != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-plug icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Charger</p>
                                                </td>
                                            @endif
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-cog icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->type == 'mpv')
                                                        MPV
                                                    @elseif ($item->type == 'city')
                                                        City
                                                    @else
                                                        Heathback
                                                    @endif
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">{{ $item->seat }} Orang</p>
                                            </td>
                                            @if ($item->audio != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-volume-up icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Audio</p>
                                                </td>
                                            @endif
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-key icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->transmisi == 'manual')
                                                        Manual
                                                    @else
                                                        Automatic
                                                    @endif
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if ($item->p3k != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-plus-square icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">P3K</p>
                                                </td>
                                            @endif
                                            @if ($item->ac != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-snowflake-o icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Ac</p>
                                                </td>
                                            @endif
                                            <!-- <td>p3k</td> <td>Ac</td> -->
                                        </tr>
                                    </table>
                                </div>
                                {{-- <div class="tab-pane pane-harga fade tabel-spek" id="v-pills-profile-{{ $key }}">
                                    <div class="tabel-harga">
                                        @if ($item->kunci_12 != 0 || $item->kunci_full != 0)
                                            <p>Lepas Kunci</p>
                                            <ul>
                                                @if ($item->kunci_12 != 0)
                                                    <li>Rp {{ number_format($item->kunci_12, 0, ',', '.') }}/ 12 jam</li>
                                                @endif
                                                @if ($item->kunci_full != 0)
                                                    <li>Rp {{ number_format($item->kunci_full, 0, ',', '.') }}/ Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->sopir_12 != 0 || $item->sopir_full != 0)
                                            <p>Dengan Sopir</p>
                                            <ul>
                                                @if ($item->sopir_12 != 0)
                                                    <li>Rp {{ number_format($item->sopir_12, 0, ',', '.') }}/ 12 jam</li>
                                                @endif
                                                @if ($item->sopir_full != 0)
                                                    <li>Rp {{ number_format($item->sopir_full, 0, ',', '.') }}/ Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->all_12 != 0 || $item->all_full != 0)
                                            <p>Sopir + BBM</p>
                                            <ul>
                                                @if ($item->all_12 != 0)
                                                    <li>Rp {{ number_format($item->all_12, 0, ',', '.') }}/ 12 jam</li>
                                                @endif
                                                @if ($item->all_full != 0)
                                                    <li>Rp {{ number_format($item->all_full, 0, ',', '.') }}/ Fullday</li>
                                                @endif
                                            </ul>
                                        @endif
                                    </div>
                                </div> --}}
                                <div
                                    class="tab-pane pane-harga-{{ $key }} fade tabel-spek"id="v-pills-profile-{{ $key }}">
                                    <style>
                                        .pane-harga-{{ $key }} {
                                            margin-bottom: 85px;
                                        }

                                        @media screen and (max-width: 767px) {
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 280px
                                            }
                                        }
                                    </style>
                                    @if ($item->kunci_12 == 0 && $item->kunci_full == 0)
                                        <style>
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 0px
                                            }

                                            @media screen and (max-width: 767px) {
                                                .pane-harga-{{ $key }} {
                                                    margin-bottom: 190px
                                                }
                                            }
                                        </style>
                                    @elseif ($item->sopir_12 == 0 && $item->sopir_full == 0)
                                        <style>
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 0px
                                            }

                                            @media screen and (max-width: 767px) {
                                                .pane-harga-{{ $key }} {
                                                    margin-bottom: 190px
                                                }
                                            }
                                        </style>
                                    @endif
                                    <div class="tabel-harga">
                                        @if ($item->kunci_12 != 0 || $item->kunci_full != 0)
                                            <p>Lepas Kunci</p>
                                            <ul>
                                                @if ($item->kunci_12 != 0)
                                                    <li>Rp {{ number_format($item->kunci_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->kunci_full != 0)
                                                    <li>Rp {{ number_format($item->kunci_full, 0, ',', '.') }} / Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->sopir_12 != 0 || $item->sopir_full != 0)
                                            <p>Dengan Sopir</p>
                                            <ul>
                                                @if ($item->sopir_12 != 0)
                                                    <li>Rp {{ number_format($item->sopir_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->sopir_full != 0)
                                                    <li>Rp {{ number_format($item->sopir_full, 0, ',', '.') }} / Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->all_12 != 0 || $item->all_full != 0)
                                            <p>Sopir + BBM</p>
                                            <ul>
                                                @if ($item->all_12 != 0)
                                                    <li>Rp {{ number_format($item->all_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->all_full != 0)
                                                    <li>Rp {{ number_format($item->all_full, 0, ',', '.') }} / Fullday</li>
                                                @endif
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="https://api.whatsapp.com/send?phone=+62{{ Str::substr($profil->kontak, 1, 200) }}&text={{ $profil->text_wa }}"
                        target="_blank">
                        <button class="pesan-now">
                            <p class="pesan">Pesan Sekarang</p>
                        </button>
                    </a>
                </div>
            </div>

            <br>
            <br>
        @endforeach


        <a href="{{ route('sewa-mobil') }}">
            <button class="all-btn">
                <p class="view-all"></p>Lihat Semua
            </button>
        </a>
    </div>

    {{-- TESTIOMNI --}}
    <div class="bg-promo">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-top">
                    <p class="title-1 syarat">Testimoni Customer Astha Trans </p>
                    <p class="deskripsi-1">Testimoni para Customer yang sudah menggunakan jasa layanan transportasi dan
                        wisata Astha Trans Melalui
                        Rating Dari Google Reviews.</p>
                    <hr class="garis-1">
                </div>
                <div id="priview" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($testimoni as $key => $item)
                            <li data-target="#priview" data-slide-to="{{ $key }}"
                                class="{{ $key == 0 ? 'active' : '' }}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($testimoni as $key => $item)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="testimonial4_slide">
                                    <img class="img-circle img-responsive" src="{{ asset('uploads/' . $item->image) }}"
                                        alt="First slide">
                                    <p>{{ $item->deskripsi }}</p>
                                    <h4>{{ $item->name }}</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <br>
                    <a class="carousel-control-prev" href="#priview" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#priview" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('assets/img/memilih.png') }}" class="img-memilih center" alt="">
            </div>
            <div class="col-md-6">
                <p class="title-memilih">Kenapa Memilih Astha Trans ?</p>
                <p class="text-memilih">Karena kami siap melayani private trip, family trip, atau group baik itu dari
                    liburan, kegiatan kerja, ataupun acara lainnya. Dengan armada terbaru dan pastinya terawat, didukung
                    oleh driver-driver yang profesional dan berpengalaman. Memastikan perjalanan Anda aman dan nyaman.</p>
                <ul class="ul-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada / mobil terawat dengan
                        baik
                    </li>
                    <hr class="gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Driver Berpengalaman</li>
                    <hr class=" gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada Terpercaya</li>
                    <hr class=" gr-memilih">
                </ul>
            </div>
        </div>
        <br>
    </div>
@endsection
@section('include-js')
    <!-- faq -->
    <script
        src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-1b93190375e9ccc259df3a57c1abc0e64599724ae30d7ea4c6877eb615f89387.js">
    </script>
    <script id="rendered-js">
        const items = document.querySelectorAll(".accordion button");

        function toggleAccordion() {
            const itemToggle = this.getAttribute('aria-expanded');
            for (i = 0; i < items.length; i++) {
                if (window.CP.shouldStopExecution(0)) break;
                items[i].setAttribute('aria-expanded', 'false');
            }
            window.CP.exitedLoop(0);
            if (itemToggle == 'false') {
                this.setAttribute('aria-expanded', 'true');
            }
        }
        items.forEach(item => item.addEventListener('click', toggleAccordion));
    </script>
    <!-- faq -->
@endsection
