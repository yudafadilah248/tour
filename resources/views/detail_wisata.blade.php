@section('include-css')
@endsection
@extends('layouts.master')
@section('page_content')
    <div class="banner pt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="judul">Detail {{ $tour->tour_categori->name }}</p>
                    <p class="deskripsi">Astha Trans menawarkan {{ $tour->tour_categori->name }} untuk Anda yang ingin
                        berlibur bersama
                        kerabat, pasangan, keluarga, rekan kerja atau bisnis. Selain {{ $tour->tour_categori->name }} yang
                        kami tawarkan,
                        Anda juga bisa membuat list tempat wisata sendiri. Kami siap melayani untuk transportasi dan
                        kebutuhan lainnya untuk wisata Anda.</p>
                </div>
                <div class="col-md-6 block">
                    <img src="{{ asset('assets/img/wisata.png') }}" class="img-head" width="100%" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="kotak-wisata">
                    @if ($tour->diskon != 0)
                        <style>
                            @media screen and (max-width: 575px) {
                                .title-custom {
                                    width: 200px;
                                }

                                .diskon-wisata {
                                    margin-top: -75px;
                                }
                            }
                        </style>
                    @endif
                    <div class="title-wisata  @if ($tour->diskon != 0) title-custom @endif ">
                        {{ $tour->name }} - {{ $tour->tour_categori->name }}
                    </div>
                    @if ($tour->diskon != 0)
                        <div class="diskon-wisata">Diskon {{ $tour->diskon }}%</div>
                    @endif
                    <img src="{{ asset('uploads/' . $tour->image) }}" alt="" class="img-wisata">
                    @php
                        $cari_diskon = ($tour->diskon / 100) * $tour->start_price;
                        $diskon = $tour->start_price - $cari_diskon;
                        // echo $diskon;
                    @endphp
                    @if ($tour->diskon != 0)
                        <div class="harga-wisata">Harga Mulai : &nbsp;<strike style="color: #c1c1c1cc;"> Rp.
                                {{ number_format($tour->start_price, 0, ',', '.') }} </strike> &nbsp;
                            Rp. {{ number_format($diskon, 0, ',', '.') }}
                        </div>
                    @else
                        <div class="harga-wisata">Harga Mulai : Rp. {{ number_format($tour->start_price, 0, ',', '.') }}
                        </div>
                    @endif

                    <ul class="nav nav-tabs " role="tablist">
                        <li class="pt-wisata" role="pt">
                            <div class="btn-wisata active" id="tempat-tab" data-toggle="pill" data-target="#tempat"
                                role="tab" type="button" aria-controls="tempat" aria-selected="true">Tempat
                                Wisata</div>
                        </li>
                        <li class="pt-wisata" role="pt">
                            <div class="btn-wisata" id="harga-tab" data-toggle="pill" data-target="#harga" role="tab"
                                type="button" aria-controls="harga" aria-selected="false">Detail
                                Harga</div>
                        </li>
                        <li class="pt-wisata" role="pt">
                            <div class="btn-wisata" id="itenary-tab" data-toggle="pill" data-target="#itenary"
                                role="tab" type="button" aria-controls="itenary" aria-selected="false">Itenary
                            </div>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tempat" role="tabpanel" aria-labelledby="tempat-tab">
                            <div class="destinasi">
                                {!! $tour->wisata !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="harga" role="tabpanel" aria-labelledby="harga-tab">
                            <table class="table table-striped table-bordered" border="1">
                                <thead>
                                    <tr>
                                        <th>Jumlah Peserta</th>
                                        <th>Harga/pax</th>
                                    </tr>
                                </thead>
                                <tbody class="list-destinasti">
                                    @foreach ($d_dewasa->tour_price as $item)
                                        <tr>
                                            <td>{{ $item->jumlah }}</td>
                                            @php
                                                $cari_diskon = ($tour->diskon / 100) * $item->price;
                                                $diskon = $item->price - $cari_diskon;
                                                // echo $diskon;
                                            @endphp
                                            @if ($tour->diskon != 0)
                                                <td>
                                                    <strike style="color: rgb(151,151,151);"> Rp.
                                                        {{ number_format($item->price, 0, ',', '.') }} </strike> &nbsp;
                                                    Rp. {{ number_format($diskon, 0, ',', '.') }}
                                                </td>
                                            @else
                                                <td>Rp. {{ number_format($item->price, 0, ',', '.') }}
                                            @endif
                                            {{-- <td>Min 2 - 3 Org</td>
                                            <td>Rp. 620.000</td> --}}
                                        </tr>
                                    @endforeach
                            </table>
                            {{-- <table class="table table-striped table-bordered" border="1">
                                <thead>
                                    <tr>
                                        <th>Anak (3 - 12 Yrs)</th>
                                        <th>Harga/pax</th>
                                    </tr>
                                </thead>
                                <tbody class="list-destinasti">
                                    @foreach ($d_anak->tour_price as $item)
                                        <tr>
                                            <td>{{ $item->jumlah }}</td>
                                            <td>Rp. {{ number_format($item->price, 0, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table class="table table-striped table-bordered" border="1">
                                <thead>
                                    <tr>
                                        <th>Infant/Balita (1 - 2 Yrs)</th>
                                        <th>Harga/pax</th>
                                    </tr>
                                </thead>
                                <tbody class="list-destinasti">
                                    @foreach ($d_balita->tour_price as $item)
                                        <tr>
                                            <td>{{ $item->jumlah }}</td>
                                            <td>Rp. {{ number_format($item->price, 0, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table> --}}
                        </div>
                        <div class="tab-pane fade" id="itenary" role="tabpanel" aria-labelledby="itenary-tab">
                            {{-- <p class="title-itenary">Hari Pertama:</p> --}}
                            <div class="itenary">
                                {!! $tour->itenary !!}
                            </div>
                        </div>
                    </div>
                    <a href="https://api.whatsapp.com/send?phone=+62{{ Str::substr($profil->kontak, 1, 200) }}&text={{ $profil->text_wa }}"
                        target="_blank">
                        <button class="pesan-wisata">
                            Pesan Sekarang
                        </button>
                    </a>

                </div>

            </div>

            <div class="col-md-3">
                <div class="bar-detail">Paket Wisata Jogja</div>
                <ul class="ul-bar-detail">
                    @foreach ($paket as $item)
                        <li class="li-bar-detail"> <a href="{{ route('detail-wisata', $item->slug) }}"
                                class="bar-color">{{ $item->name }}</a></li>
                        <hr class="gr-bar-detail">
                    @endforeach
                </ul>
                <div class="bar-detail">Travel Blog</div>
                <ul class="ul-bar-detail">
                    @foreach ($blog as $item)
                        <li class="li-bar-blog"> <a href="{{ route('blog.detail-blog', $item->slug) }}"
                                class="bar-color">{{ $item->judul }}</a>
                        </li>
                        <hr class="gr-bar-detail">
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    {{-- TOP --}}
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-top">
                <p class="title-1">Top Penyewaan Mobil Kami</p>
                <p class="deskripsi-1">Astha Trans mengutamakan kenyaman para pelanggan dengan unit terbaru dan terawat,
                    ditambah dengan tenaga driver kami yang berpengalaman dan harga sewa yang terjangkau.</p>
                <hr class="garis-1">
            </div>
        </div>

        @foreach ($top_car as $key => $item)
            <div class="row-2">
                <div class="kotak">
                    @if ($item->diskon != 0)
                        <div class="diskon">{{ $item->diskon }}%</div>
                    @endif
                    <div class="col-md-6">
                        <p class="name-car">{{ $item->name }}</p>
                        <img src="{{ asset('uploads/' . $item->image) }}" class="image-car" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="posisi">
                            <div class="box-price">
                                Harga Mulai : Rp. {{ number_format($item->harga, 0, ',', '.') }} / Full Day
                            </div>
                            <div class="navbar justify-content-center">
                                <div class="nav">
                                    <button class="box-spek active" id="v-pills-home-{{ $key }}-tab"
                                        data-toggle="pill" data-target="#v-pills-home-{{ $key }}"
                                        role="tab" aria-controls="v-pills-home-{{ $key }}"
                                        aria-selected="true">Spesifikasi</button>
                                    <button class="box-detail" id="v-pills-profile-{{ $key }}-tab"
                                        data-toggle="pill" data-target="#v-pills-profile-{{ $key }}"
                                        role="tab" aria-controls="v-pills-profile-{{ $key }}"
                                        aria-selected="false">Detail
                                        Harga</button>
                                </div>
                            </div>
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active custom" id="v-pills-home-{{ $key }}"
                                    aria-labelledby="v-pills-home-{{ $key }}-tab">
                                    <table class="tabel-spek">
                                        <tr>
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-tint icon-spek"
                                                            aria-hidden="true" style="margin-left: 7px;"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->bensin == 'pertalite')
                                                        Pertalite
                                                    @elseif ($item->bensin == 'pertamax')
                                                        Pertamax
                                                    @else
                                                        Solar
                                                    @endif
                                                </p>
                                            </td>
                                            @if ($item->charger != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-plug icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Charger</p>
                                                </td>
                                            @endif
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-cog icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->type == 'mpv')
                                                        MPV
                                                    @elseif ($item->type == 'city')
                                                        City
                                                    @else
                                                        Heathback
                                                    @endif
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">{{ $item->seat }} Orang</p>
                                            </td>
                                            @if ($item->audio != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-volume-up icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Audio</p>
                                                </td>
                                            @endif
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-key icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->transmisi == 'manual')
                                                        Manual
                                                    @else
                                                        Automatic
                                                    @endif
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if ($item->p3k != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-plus-square icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">P3K</p>
                                                </td>
                                            @endif
                                            @if ($item->ac != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-snowflake-o icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Ac</p>
                                                </td>
                                            @endif
                                            <!-- <td>p3k</td> <td>Ac</td> -->
                                        </tr>
                                    </table>
                                </div>
                                {{-- <div class="tab-pane pane-harga fade tabel-spek" id="v-pills-profile-{{ $key }}">
                                    <div class="tabel-harga">
                                        @if ($item->kunci_12 != 0 || $item->kunci_full != 0)
                                            <p>Lepas Kunci</p>
                                            <ul>
                                                @if ($item->kunci_12 != 0)
                                                    <li>Rp {{ number_format($item->kunci_12, 0, ',', '.') }}/ 12 jam</li>
                                                @endif
                                                @if ($item->kunci_full != 0)
                                                    <li>Rp {{ number_format($item->kunci_full, 0, ',', '.') }}/ Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->sopir_12 != 0 || $item->sopir_full != 0)
                                            <p>Dengan Sopir</p>
                                            <ul>
                                                @if ($item->sopir_12 != 0)
                                                    <li>Rp {{ number_format($item->sopir_12, 0, ',', '.') }}/ 12 jam</li>
                                                @endif
                                                @if ($item->sopir_full != 0)
                                                    <li>Rp {{ number_format($item->sopir_full, 0, ',', '.') }}/ Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->all_12 != 0 || $item->all_full != 0)
                                            <p>Sopir + BBM</p>
                                            <ul>
                                                @if ($item->all_12 != 0)
                                                    <li>Rp {{ number_format($item->all_12, 0, ',', '.') }}/ 12 jam</li>
                                                @endif
                                                @if ($item->all_full != 0)
                                                    <li>Rp {{ number_format($item->all_full, 0, ',', '.') }}/ Fullday</li>
                                                @endif
                                            </ul>
                                        @endif
                                    </div>
                                </div> --}}
                                <div
                                    class="tab-pane pane-harga-{{ $key }} fade tabel-spek"id="v-pills-profile-{{ $key }}">
                                    <style>
                                        .pane-harga-{{ $key }} {
                                            margin-bottom: 85px;
                                        }

                                        @media screen and (max-width: 767px) {
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 280px
                                            }
                                        }
                                    </style>
                                    @if ($item->kunci_12 == 0 && $item->kunci_full == 0)
                                        <style>
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 0px
                                            }

                                            @media screen and (max-width: 767px) {
                                                .pane-harga-{{ $key }} {
                                                    margin-bottom: 190px
                                                }
                                            }
                                        </style>
                                    @elseif ($item->sopir_12 == 0 && $item->sopir_full == 0)
                                        <style>
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 0px
                                            }

                                            @media screen and (max-width: 767px) {
                                                .pane-harga-{{ $key }} {
                                                    margin-bottom: 190px
                                                }
                                            }
                                        </style>
                                    @endif
                                    <div class="tabel-harga">
                                        @if ($item->kunci_12 != 0 || $item->kunci_full != 0)
                                            <p>Lepas Kunci</p>
                                            <ul>
                                                @if ($item->kunci_12 != 0)
                                                    <li>Rp {{ number_format($item->kunci_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->kunci_full != 0)
                                                    <li>Rp {{ number_format($item->kunci_full, 0, ',', '.') }} / Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->sopir_12 != 0 || $item->sopir_full != 0)
                                            <p>Dengan Sopir</p>
                                            <ul>
                                                @if ($item->sopir_12 != 0)
                                                    <li>Rp {{ number_format($item->sopir_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->sopir_full != 0)
                                                    <li>Rp {{ number_format($item->sopir_full, 0, ',', '.') }} / Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->all_12 != 0 || $item->all_full != 0)
                                            <p>Sopir + BBM</p>
                                            <ul>
                                                @if ($item->all_12 != 0)
                                                    <li>Rp {{ number_format($item->all_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->all_full != 0)
                                                    <li>Rp {{ number_format($item->all_full, 0, ',', '.') }} / Fullday</li>
                                                @endif
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="https://api.whatsapp.com/send?phone=+62{{ Str::substr($profil->kontak, 1, 200) }}&text={{ $profil->text_wa }}"
                        target="_blank">
                        <button class="pesan-now">
                            <p class="pesan">Pesan Sekarang</p>
                        </button>
                    </a>
                </div>
            </div>

            <br>
            <br>
        @endforeach


        <a href="{{ route('sewa-mobil') }}">
            <button class="all-btn">
                <p class="view-all"></p>Lihat Semua
            </button>
        </a>
    </div>

    {{-- TESTIMONI --}}
    <div class="bg-promo">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-top">
                    <p class="title-1 syarat">Testimoni Customer Astha Trans </p>
                    <p class="deskripsi-1">estimoni para Customer yang sudah menggunakan jasa layanan transportasi dan
                        wisata Astha Trans Melalui
                        Rating Dari Google Reviews.</p>
                    <hr class="garis-1">
                </div>
                <div id="priview" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($testimoni as $key => $item)
                            <li data-target="#priview" data-slide-to="{{ $key }}"
                                class="{{ $key == 0 ? 'active' : '' }}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($testimoni as $key => $item)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="testimonial4_slide">
                                    <img class="img-circle img-responsive" src="{{ asset('uploads/' . $item->image) }}"
                                        alt="First slide">
                                    <p>{{ $item->deskripsi }}</p>
                                    <h4>{{ $item->name }}</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <br>
                    <a class="carousel-control-prev" href="#priview" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#priview" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('assets/img/memilih.png') }}" class="img-memilih center" alt="">
            </div>
            <div class="col-md-6">
                <p class="title-memilih">Kenapa Memilih Astha Trans ?</p>
                <p class="text-memilih">Karena kami siap melayani private trip, family trip, atau group baik itu dari
                    liburan, kegiatan kerja, ataupun acara lainnya. Dengan armada terbaru dan pastinya terawat, didukung
                    oleh driver-driver yang profesional dan berpengalaman. Memastikan perjalanan Anda aman dan nyaman.</p>
                <ul class="ul-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada / mobil terawat dengan
                        baik
                    </li>
                    <hr class="gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Driver Berpengalaman</li>
                    <hr class=" gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada Terpercaya</li>
                    <hr class=" gr-memilih">
                </ul>
            </div>
        </div>
        <br>
    </div>
@endsection
@section('include-js')
@endsection
