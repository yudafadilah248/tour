<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/media.css') }}">
    <link rel="icon" href="{{ asset('assets/img/logo.png') }}">
</head>

<body>
    <nav class="navbar pt navbar-expand-lg sticky-top">
        <a href="index.html">
            <img class="navbar-brand" src="{{ asset('assets/img/logo.png') }}" alt="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="fa fa-align-justify iconStyle"></i></span>
        </button>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.html">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sewa_mobil.html">Sewa Mobil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sewa_bus.html">Sewa Bus</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="paket_wisata.html">Wisata</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="galeri.html">Galeri</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about.html">Tentang Kami</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="kontak.html">Kontak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="blog.html">Travel Blog</a>
                </li>
                <!-- <li class="nav-item">
          <a class="nav-link" href="https://wa.me/6289674555439">Pesan</a>
        </li> -->
            </ul>
        </div>
    </nav>

    <div class="banner pt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="judul">Mice</p>
                    <p class="deskripsi">Astha Mice adalah salah satu layanan dari perusahaan kami yang bergerak di
                        bidang Event Organizer, Tour & Travel, Pengadaan Barang dan Jasa. Berlokasi di Yogyakarta, Astha
                        Mice berkomitmen memberikan layanan yang terbaik untuk semua kalangan, baik instansi pemerintah
                        atau BUMN, maupun swasta dan personal.
                    </p>
                </div>
                <div class="col-md-6 block">
                    <img src="assets/img/wisata.png" class="img-head" width="100%" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="container mb-4">
        <div class="col-md-12 mice">
            <p class="title-1">MICE</p>
            <div class="text-mice">
                <p>
                    Astha Mice adalah salah satu layanan dari perusahaan kami yang bergerak di bidang Event Organizer,
                    Tour
                    & Travel, Pengadaan Barang dan Jasa. Berlokasi di Yogyakarta, Astha Mice berkomitmen memberikan
                    layanan
                    yang terbaik untuk semua kalangan, baik instansi pemerintah atau BUMN, maupun swasta dan personal.
                </p>
                <p>
                    Astha Mice berpengalaman menangani kegiatan di berbagai pelosok nusantara, dengan didukung berbagai
                    sumberdaya yang prima dan handal, Simplemice siap melayani anda.
                </p>
            </div>
            <hr class="garis-1">
        </div>

        <div class="box-mice">
            <div class="box-text">
                Gathering Planner
            </div>
        </div>
        <div class="mice-content">
            <ul>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a viverra mi morbi sit
                    tincidunt. Tincidunt morbi a</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a viverra mi morbi sit
                    tincidunt. Tincidunt morbi a</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a viverra mi morbi sit
                    tincidunt. Tincidunt morbi a</li>
            </ul>
        </div>
        <br>

        <div class="box-mice">
            <div class="box-text">
                Meeting Planner
            </div>
        </div>
        <div class="mice-content">
            <ul>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a viverra mi morbi sit
                    tincidunt. Tincidunt morbi a</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a viverra mi morbi sit
                    tincidunt. Tincidunt morbi a</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a viverra mi morbi sit
                    tincidunt. Tincidunt morbi a</li>
            </ul>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-fasilitas">
                        Lokasi Acara
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-fasilitas">
                        Fasilitas
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-fasilitas">
                        Transportasi
                    </div>
                </div>
            </div>
        </div>

        <a href="#">
            <button class="pesan-mice">
                Pesan Sekarang
            </button>
        </a>



    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-top">
                <p class="title-1">Top Penyewaan Mobil Kami</p>
                <p class="deskripsi-1">Astha Trans Jogja menyediakan layanan jasa sewa mobil Jogja dengan harga yang
                    sangat
                    ekonomis
                    dengan unit baru dan terawat. Tersedia juga layanan sewa mobil lepas kunci atau All in (Driver +
                    Bbm)
                    Dengan syarat yang mudah </p>
                <hr class="garis-1">
            </div>
        </div>

        <div class="row-2">
            <div class="kotak">
                <div class="diskon">50%</div>
                <div class="col-md-6">
                    <p class="name-car">Xenia Daihatsu</p>
                    <img src="{{ asset('assets/img/car.png') }}" class="image-car" alt="">
                </div>
                <div class="col-md-6">
                    <div class="posisi">
                        <div class="box-price">
                            Harga Mulai : Rp. 350.000 / Full Day
                        </div>
                        <div class="navbar justify-content-center">
                            <div class="nav">
                                <button class="box-spek active" id="v-pills-home-tab" data-toggle="pill"
                                    data-target="#v-pills-home" role="tab" aria-controls="v-pills-home"
                                    aria-selected="true">Spesifikasi</button>
                                <button class="box-detail" id="v-pills-profile-tab" data-toggle="pill"
                                    data-target="#v-pills-profile" role="tab" aria-controls="v-pills-profile"
                                    aria-selected="false">Detail
                                    Harga</button>
                            </div>
                        </div>
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active custom" id="v-pills-home"
                                aria-labelledby="v-pills-home-tab">
                                <table class="tabel-spek">
                                    <tr>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Pertalite</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-key icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Charger</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">MPV</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">5 Orang</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Audio</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Automatic </p>
                                        </td> <!-- <br>/ Manual -->
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">P3K</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Ac</p>
                                        </td>
                                        <!-- <td>p3k</td>
                        <td>Ac</td> -->
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane pane-harga fade tabel-spek" id="v-pills-profile">
                                <div class="tabel-harga">
                                    <p>Lepas Kunci</p>
                                    <ul>
                                        <li>Rp 275.000/ 12 jam</li>
                                        <li>Rp 350.000/ Fullday</li>
                                    </ul>
                                    <p>Dengan Sopir</p>
                                    <ul>
                                        <li>Rp 400.000/ 12 jam</li>
                                        <li>Rp 450.000/ Fullday</li>
                                    </ul>
                                    <p>Sopir + BBM</p>
                                    <ul>
                                        <li>Rp 550.000/ 12 jam</li>
                                        <li>Rp 650.000/ Fullday</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="pesan-now">
                    <p class="pesan">Pesan Sekarang</p>
                </button>
            </div>
        </div>

        <br>
        <br>

        <div class="row-2">
            <div class="kotak">
                <div class="diskon">50%</div>
                <div class="col-md-6">
                    <p class="name-car">Xenia Daihatsu</p>
                    <img src="{{ asset('assets/img/car.png') }}" class="image-car" alt="">
                </div>
                <div class="col-md-6">
                    <div class="posisi">
                        <div class="box-price">
                            Harga Mulai : Rp. 350.000 / Full Day
                        </div>
                        <div class="navbar justify-content-center">
                            <div class="nav">
                                <button class="box-spek active" id="v-pills-home-tab" data-toggle="pill"
                                    data-target="#v-pills-home" role="tab" aria-controls="v-pills-home"
                                    aria-selected="true">Spesifikasi</button>
                                <button class="box-detail" id="v-pills-profile-tab" data-toggle="pill"
                                    data-target="#v-pills-profile" role="tab" aria-controls="v-pills-profile"
                                    aria-selected="false">Detail
                                    Harga</button>
                            </div>
                        </div>
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active custom" id="v-pills-home"
                                aria-labelledby="v-pills-home-tab">
                                <table class="tabel-spek">
                                    <tr>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Pertalite</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-key icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Charger</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">MPV</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">5 Orang</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Audio</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Automatic </p>
                                        </td> <!-- <br>/ Manual -->
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">P3K</p>
                                        </td>
                                        <td>
                                            <div class="bulat"><span><i class="fa fa-user icon-spek"
                                                        aria-hidden="true"></i></span></div>
                                            <p class="spek">Ac</p>
                                        </td>
                                        <!-- <td>p3k</td>
                        <td>Ac</td> -->
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane pane-harga fade tabel-spek" id="v-pills-profile">
                                <div class="tabel-harga">
                                    <p>Lepas Kunci</p>
                                    <ul>
                                        <li>Rp 275.000/ 12 jam</li>
                                        <li>Rp 350.000/ Fullday</li>
                                    </ul>
                                    <p>Dengan Sopir</p>
                                    <ul>
                                        <li>Rp 400.000/ 12 jam</li>
                                        <li>Rp 450.000/ Fullday</li>
                                    </ul>
                                    <p>Sopir + BBM</p>
                                    <ul>
                                        <li>Rp 550.000/ 12 jam</li>
                                        <li>Rp 650.000/ Fullday</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="pesan-now">
                    <p class="pesan">Pesan Sekarang</p>
                </button>
            </div>
        </div>
        <br>
        <br>
        <button class="all-btn">
            <p class="view-all"></p>Lihat Semua
        </button>
    </div>

    <div class="bg-promo">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-top">
                    <p class="title-1 syarat">Testimoni Customer Astha Trans </p>
                    <p class="deskripsi-1">Testimoni para Customer yang sudah menggunakan jasa layanan travel and
                        tour
                        Astha Trans
                        Melalui
                        Rating Dari Google Reviews</p>
                    <hr class="garis-1">
                </div>
                <div id="priview" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#priview" data-slide-to="0" class="active"></li>
                        <li data-target="#priview" data-slide-to="1"></li>
                        <li data-target="#priview" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="testimonial4_slide">
                                <img class="img-circle img-responsive"
                                    src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_182ac47fd7b%20text%20%7B%20fill%3A%23333%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_182ac47fd7b%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23555%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22276.9921875%22%20y%3D%22217.7%22%3EThird%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                    alt="First slide">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown
                                    printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                                <h4>Client 2</h4>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="testimonial4_slide">
                                <img class="img-circle img-responsive"
                                    src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_182ac47fd78%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_182ac47fd78%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.9140625%22%20y%3D%22217.7%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                    alt="Second slide">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown
                                    printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                                <h4>Client 2</h4>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="testimonial4_slide">
                                <img class="img-circle img-responsive"
                                    src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_182ac47fd7a%20text%20%7B%20fill%3A%23444%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_182ac47fd7a%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23666%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22247.3125%22%20y%3D%22217.7%22%3ESecond%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                    alt="Third slide">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown
                                    printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                                <h4>Client 2</h4>
                            </div>
                        </div>
                    </div>
                    <br>
                    <a class="carousel-control-prev" href="#priview" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#priview" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('assets/img/memilih.png') }}" class="img-memilih center" alt="">
            </div>
            <div class="col-md-6">
                <p class="title-memilih">Kenapa Memilih Astha Trans ?</p>
                <p class="text-memilih">Liburan bersama rombongan atau keluarga besar dengan sewa bus pariwisata
                    terbaru
                    pasti jadi lebih menyenangkan. Dilengkapi fasilita terbaik, armada terawat, dan bus pariwisata
                    berbagai ukuran, Salsa Wisata siap mengantar tour wisata rombongan sekolah, perusahaan, atau
                    keluarga besar Anda dengan nyaman dan aman</p>
                <ul class="ul-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada / mobil terawat dengan baik
                    </li>
                    <hr class="gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Driver Berpengalaman</li>
                    <hr class=" gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada Terpercaya</li>
                    <hr class=" gr-memilih">
                </ul>
            </div>
        </div>
        <br>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="assets/img/memilih.png" class="img-memilih center" alt="">
            </div>
            <div class="col-md-6">
                <p class="title-memilih">Kenapa Memilih Astha Trans ?</p>
                <p class="text-memilih">Liburan bersama rombongan atau keluarga besar dengan sewa bus pariwisata
                    terbaru
                    pasti jadi lebih menyenangkan. Dilengkapi fasilita terbaik, armada terawat, dan bus pariwisata
                    berbagai ukuran, Salsa Wisata siap mengantar tour wisata rombongan sekolah, perusahaan, atau
                    keluarga besar Anda dengan nyaman dan aman</p>
                <ul class="ul-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada / mobil terawat dengan baik
                    </li>
                    <hr class="gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Driver Berpengalaman</li>
                    <hr class=" gr-memilih">
                    <li class="li-memilih"><i class="fa fa-check color-check"></i>Armada Terpercaya</li>
                    <hr class=" gr-memilih">
                </ul>
            </div>
        </div>
        <br>
    </div>

    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p class="title-fot">Tentang Kami</p>
                    <p class="text-fot">In publishing and graphic design,
                        Lorem ipsum is a placeholder text
                        commonly used to demonstrate
                        the visual form of a document or
                        a typeface without relying on mean
                        ingful content. Lorem ipsum may
                        be used as a placeholder before final </p>
                </div>
                <div class="col-md-4">
                    <p class="title-fot">Kontak</p>
                    <p class="kontak-fot">089674555439</p>
                    <p class="kontak-fot">Isntagram</p>
                    <p class="kontak-fot">Facebook</p>
                    <p class="title-fot">Alamat</p>
                    <p class="alamat-fot">Jl. Kebun Raya No.2, Rejowinangun,
                        Kec. Kotagede, Kota Yogyakarta,
                        Daerah Istimewa Yogyakarta 55171</p>
                </div>
                <div class="col-md-4">
                    <p class="title-fot">Maps</p>
                    <div class="embed-responsive embed-responsive-16by9 maps">
                        <iframe class="embed-responsive-item"
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6523.972232754733!2d33.313468!3d35.156967!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x552d685f196ffa7c!2zzpnOkc6kzqHOmc6azp8gzprOlc6dzqTOoc6fICLOnyDOkc6gzp_Oo86kzp_Om86fzqMgzpvOn86lzprOkc6jIg!5e0!3m2!1sen!2sus!4v1525091450028"
                            allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            Copyright @ 2022 Astha Trans
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
    </script>
</body>

</html>
