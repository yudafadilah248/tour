<nav class="navbar pt navbar-expand-lg sticky-top">
    <a href="{{ route('home') }}">
        <img class="navbar-brand" src="{{ asset('assets/img/logo.png') }}" alt="logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
        aria-expanded="false" aria-label="Toggle navigation">
        <span><i class="fa fa-align-justify iconStyle"></i></span>
    </button>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navTrans" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Astha Trans
                </a>
                <div class="dropdown-menu" aria-labelledby="navTrans">
                    <a class="dropdown-item" href="{{ route('sewa-mobil') }}">Sewa Mobil</a>
                    <a class="dropdown-item" href="{{ route('sewa-bus') }}">Sewa Bus</a>
                </div>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ route('sewa-mobil') }}">Sewa Mobil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('sewa-bus') }}">Sewa Bus</a>
            </li> --}}
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navMice" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Astha Mice
                </a>
                <div class="dropdown-menu" aria-labelledby="navMice">
                    <a class="dropdown-item" href="{{ route('mice') }}">Mice</a>
                    <a class="dropdown-item" href="{{ route('mice-event') }}">Mice Event</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('paket-wisata') }}">Wisata</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('galeri') }}">Galeri</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('about') }}">Tentang Kami</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('kontak') }}">Kontak</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('blog') }}">Travel Blog</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ route('mice') }}">Mice</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('mice-event') }}">Mice Event</a>
            </li> --}}
        </ul>
    </div>
</nav>
