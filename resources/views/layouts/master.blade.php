<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="PT. Astha Prima Anugerah atau Astha Trans merupakan
     penyedia jasa transportasi wisata, perjalanan dinas, antar jemput bandara, dan layanan
     transportasi lainnya. Cita-cita terbesarkami adalah bisa memberikan pelayanan terbaik bagi
     para pelanggan kami." />
    <meta name="keywords"
        content="PT. Astha Prima Anugerah atau Astha Trans merupakan
    penyedia jasa transportasi wisata, perjalanan dinas, antar jemput bandara, dan layanan
    transportasi lainnya. Cita-cita terbesarkami adalah bisa memberikan pelayanan terbaik bagi
    para pelanggan kami." />
    <meta property="bb:client_area" content="https://asthatrans.com/">
    <meta name="robots" content="index, follow, noodp">
    {{-- <meta property="fb:app_id" content="979757679322175" /> --}}
    <meta property="og:url" content="https://asthatrans.com/" />
    <meta property="og:title" content="PT. Astha Prima Anugerah - asthatrans.com" />
    <meta property="og:description"
        content="PT. Astha Prima Anugerah atau Astha Trans merupakan
    penyedia jasa transportasi wisata, perjalanan dinas, antar jemput bandara, dan layanan
    transportasi lainnya. Cita-cita terbesarkami adalah bisa memberikan pelayanan terbaik bagi
    para pelanggan kami." />
    <meta property="og:image" content="{{ asset('assets/img/logo.png') }}" />
    <meta property="og:site_name" content="Infarm" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="website" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@infarmid" />
    <meta name="twitter:title" content="PT. Astha Prima Anugerah - asthatrans.com" />
    <meta name="twitter:description"
        content="PT. Astha Prima Anugerah atau Astha Trans merupakan
    penyedia jasa transportasi wisata, perjalanan dinas, antar jemput bandara, dan layanan
    transportasi lainnya. Cita-cita terbesarkami adalah bisa memberikan pelayanan terbaik bagi
    para pelanggan kami." />
    <meta name="twitter:image" content="{{ asset('assets/img/logo.png') }}" />
    <meta itemprop="description"
        content="PT. Astha Prima Anugerah atau Astha Trans merupakan
    penyedia jasa transportasi wisata, perjalanan dinas, antar jemput bandara, dan layanan
    transportasi lainnya. Cita-cita terbesarkami adalah bisa memberikan pelayanan terbaik bagi
    para pelanggan kami.">

    <title>{{ @$page_title ? $page_title . ' - ' . config('app.name') : config('app.name') }}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/media.css') }}">
    <link rel="icon" href="{{ asset('assets/img/logo.png') }}">

    @yield('include-css')

</head>

<body>

    @include('layouts._navbar')

    @yield('modals')

    @yield('page_content')

    @include('layouts._footer')

    @yield('include-js')

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
    </script>
</body>

</html>
