<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <p class="title-fot">Tentang Kami</p>
                <p class="text-fot">{{ $profil->deskripsi_singkat }} </p>
            </div>
            <div class="col-md-4">
                <p class="title-fot">Kontak</p>
                <p class="kontak-fot">
                    <a href="https://api.whatsapp.com/send?phone={{ $profil->kontak }}&text=Hallo+Bisa+Saya+Bantu"><span
                            class="header-sosmed"><i class="fab fa fa-whatsapp"></i></span>
                        {{ $profil->kontak }}</a>
                </p>
                <p class="kontak-fot">
                    <a href="{{ $profil->ig }}"><span class="header-sosmed"><i class="fab fa fa-instagram"></i></span>
                        Instagram</a>
                </p>
                <p class="kontak-fot">
                    <a href="{{ $profil->fb }}"><span class="header-sosmed facebook"><i
                                class="fab fa fa-facebook"></i></span>
                        Facebook
                    </a>
                </p>

                <p class="title-fot">Alamat</p>
                <p class="alamat-fot">{{ $profil->alamat }}</p>
            </div>
            <div class="col-md-4" style="margin-bottom: 10px">
                <p class="title-fot">Maps</p>
                <div class="embed-responsive embed-responsive-16by9 maps">
                    <iframe class="embed-responsive-item" src="{{ $profil->maps }}" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright">
    <div class="container">
        Copyright @ 2022 Astha Trans
    </div>
</div>
