@section('include-css')
@endsection
@extends('layouts.master')
@section('page_content')
    <div class="banner pt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="judul">Astha Trans</p>
                    <p class="deskripsi">PT. Astha Prima Anugerah atau Astha Trans merupakan penyedia jasa transportasi
                        wisata, perjalanan dinas, antar jemput bandara, dan layanan transportasi lainnya. Cita-cita terbesar
                        kami adalah bisa memberikan pelayanan terbaik bagi para pelanggan kami.</p>
                </div>
                <div class="col-md-6 block">
                    <img src="{{ asset('assets/img/mobil.png') }}" class="img-head" width="100%" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-top">
                <p class="title-1">Top Penyewaan Mobil Kami</p>
                <p class="deskripsi-1">Astha Trans mengutamakan kenyaman para pelanggan dengan unit terbaru dan terawat,
                    ditambah dengan tenaga driver kami yang berpengalaman dan harga sewa yang terjangkau.</p>
                <hr class="garis-1">
            </div>
        </div>

        @foreach ($top_car as $key => $item)
            <div class="row-2">
                <div class="kotak">
                    @if ($item->diskon != 0)
                        <div class="diskon">{{ $item->diskon }}%</div>
                    @endif
                    <div class="col-md-6">
                        <p class="name-car">{{ $item->name }}</p>
                        <img src="{{ asset('uploads/' . $item->image) }}" class="image-car" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="posisi">
                            <div class="box-price">
                                Harga Mulai : Rp. {{ number_format($item->harga, 0, ',', '.') }} / Full Day
                            </div>
                            <div class="navbar justify-content-center">
                                <div class="nav">
                                    <button class="box-spek active" id="v-pills-home-{{ $key }}-tab"
                                        data-toggle="pill" data-target="#v-pills-home-{{ $key }}" role="tab"
                                        aria-controls="v-pills-home-{{ $key }}"
                                        aria-selected="true">Spesifikasi</button>
                                    <button class="box-detail" id="v-pills-profile-{{ $key }}-tab"
                                        data-toggle="pill" data-target="#v-pills-profile-{{ $key }}" role="tab"
                                        aria-controls="v-pills-profile-{{ $key }}" aria-selected="false">Detail
                                        Harga</button>
                                </div>
                            </div>
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active custom" id="v-pills-home-{{ $key }}"
                                    aria-labelledby="v-pills-home-{{ $key }}-tab">
                                    <table class="tabel-spek">
                                        <tr>
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-tint icon-spek" aria-hidden="true"
                                                            style="margin-left: 7px;"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->bensin == 'pertalite')
                                                        Pertalite
                                                    @elseif ($item->bensin == 'pertamax')
                                                        Pertamax
                                                    @else
                                                        Solar
                                                    @endif
                                                </p>
                                            </td>
                                            @if ($item->charger != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-plug icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Charger</p>
                                                </td>
                                            @endif
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-cog icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->type == 'mpv')
                                                        MPV
                                                    @elseif ($item->type == 'city')
                                                        City
                                                    @else
                                                        Heathback
                                                    @endif
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-user icon-spek" aria-hidden="true"
                                                            style="margin-left: 5.5px;"></i></span></div>
                                                <p class="spek">{{ $item->seat }} Orang</p>
                                            </td>
                                            @if ($item->audio != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-volume-up icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Audio</p>
                                                </td>
                                            @endif
                                            <td>
                                                <div class="bulat"><span><i class="fa fa-key icon-spek"
                                                            aria-hidden="true"></i></span></div>
                                                <p class="spek">
                                                    @if ($item->transmisi == 'manual')
                                                        Manual
                                                    @else
                                                        Automatic
                                                    @endif
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if ($item->p3k != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-plus-square icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">P3K</p>
                                                </td>
                                            @endif
                                            @if ($item->ac != 0)
                                                <td>
                                                    <div class="bulat"><span><i class="fa fa-snowflake-o icon-spek"
                                                                aria-hidden="true"></i></span></div>
                                                    <p class="spek">Ac</p>
                                                </td>
                                            @endif
                                            <!-- <td>p3k</td> <td>Ac</td> -->
                                        </tr>
                                    </table>
                                </div>
                                <div
                                    class="tab-pane pane-harga-{{ $key }} fade tabel-spek"id="v-pills-profile-{{ $key }}">
                                    <style>
                                        .pane-harga-{{ $key }} {
                                            margin-bottom: 85px;
                                        }

                                        @media screen and (max-width: 767px) {
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 280px
                                            }
                                        }
                                    </style>
                                    @if ($item->kunci_12 == 0 && $item->kunci_full == 0)
                                        <style>
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 0px
                                            }

                                            @media screen and (max-width: 767px) {
                                                .pane-harga-{{ $key }} {
                                                    margin-bottom: 190px
                                                }
                                            }
                                        </style>
                                    @elseif ($item->sopir_12 == 0 && $item->sopir_full == 0)
                                        <style>
                                            .pane-harga-{{ $key }} {
                                                margin-bottom: 0px
                                            }

                                            @media screen and (max-width: 767px) {
                                                .pane-harga-{{ $key }} {
                                                    margin-bottom: 190px
                                                }
                                            }
                                        </style>
                                    @endif
                                    <div class="tabel-harga">
                                        @if ($item->kunci_12 != 0 || $item->kunci_full != 0)
                                            <p>Lepas Kunci</p>
                                            <ul>
                                                @if ($item->kunci_12 != 0)
                                                    <li>Rp {{ number_format($item->kunci_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->kunci_full != 0)
                                                    <li>Rp {{ number_format($item->kunci_full, 0, ',', '.') }} / Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->sopir_12 != 0 || $item->sopir_full != 0)
                                            <p>Dengan Sopir</p>
                                            <ul>
                                                @if ($item->sopir_12 != 0)
                                                    <li>Rp {{ number_format($item->sopir_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->sopir_full != 0)
                                                    <li>Rp {{ number_format($item->sopir_full, 0, ',', '.') }} / Fullday
                                                    </li>
                                                @endif
                                            </ul>
                                        @endif
                                        @if ($item->all_12 != 0 || $item->all_full != 0)
                                            <p>Sopir + BBM</p>
                                            <ul>
                                                @if ($item->all_12 != 0)
                                                    <li>Rp {{ number_format($item->all_12, 0, ',', '.') }} / 12 jam</li>
                                                @endif
                                                @if ($item->all_full != 0)
                                                    <li>Rp {{ number_format($item->all_full, 0, ',', '.') }} / Fullday</li>
                                                @endif
                                            </ul>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <a href="https://api.whatsapp.com/send?phone=+62{{ Str::substr($profil->kontak, 1, 200) }}&text={{ $profil->text_wa }}"
                        target="_blank">
                        <button class="pesan-now">
                            <p class="pesan">Pesan Sekarang</p>
                        </button>
                    </a>
                </div>
            </div>

            <br>
            <br>
        @endforeach


        <a href="{{ route('sewa-mobil') }}">
            <button class="all-btn">
                <p class="view-all"></p>Lihat Semua
            </button>
        </a>
    </div>

    <div class="bg-promo">
        @if ($diskon_car->isNotEmpty())
            <div class="container">
                <br>
                <p class="title-1">Promo Terbatas</p>
                <hr class="garis-1">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($diskon_car as $key => $item)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}"
                                class="{{ $key == 0 ? 'active' : '' }}">
                            </li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner" style="overflow: initial;">
                        @foreach ($diskon_car as $key => $item)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="row-pr d-block w-100">
                                    <div class="kotak">
                                        @if ($item->diskon != 0)
                                            <div class="diskon">{{ $item->diskon }}%</div>
                                        @endif
                                        <div class="col-md-6">
                                            <p class="name-car">{{ $item->name }}</p>
                                            <img src="{{ asset('uploads/' . $item->image) }}" class="image-car"
                                                alt="">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="posisi">
                                                <div class="box-price">
                                                    Harga Mulai : Rp. {{ number_format($item->harga, 0, ',', '.') }} / Full
                                                    Day
                                                </div>
                                                <div class="navbar justify-content-center">
                                                    <div class="nav">
                                                        <button class="box-spek active"
                                                            id="v-pills-promo-{{ $key }}-tab" data-toggle="pill"
                                                            data-target="#v-pills-promo-{{ $key }}"
                                                            role="tab"
                                                            aria-controls="v-pills-promo-{{ $key }}"
                                                            aria-selected="true">Spesifikasi</button>
                                                        <button class="box-detail"
                                                            id="v-pills-profile-1-{{ $key }}-tab"
                                                            data-toggle="pill"
                                                            data-target="#v-pills-profile-1-{{ $key }}"
                                                            role="tab"
                                                            aria-controls="v-pills-profile-1-{{ $key }}"
                                                            aria-selected="false">Detail
                                                            Harga</button>
                                                    </div>
                                                </div>
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <div class="tab-pane fade show active custom"
                                                        id="v-pills-promo-{{ $key }}"
                                                        aria-labelledby="v-pills-promo-{{ $key }}-tab">
                                                        <table class="tabel-spek">
                                                            <tr>
                                                                <td>
                                                                    <div class="bulat"><span><i
                                                                                class="fa fa-tint icon-spek"
                                                                                aria-hidden="true"
                                                                                style="margin-left: 7px;"></i></span></div>
                                                                    <p class="spek">
                                                                        @if ($item->bensin == 'pertalite')
                                                                            Pertalite
                                                                        @elseif ($item->bensin == 'pertamax')
                                                                            Pertamax
                                                                        @else
                                                                            Solar
                                                                        @endif
                                                                    </p>
                                                                </td>
                                                                @if ($item->charger != 0)
                                                                    <td>
                                                                        <div class="bulat"><span><i
                                                                                    class="fa fa-plug icon-spek"
                                                                                    aria-hidden="true"></i></span></div>
                                                                        <p class="spek">Charger</p>
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    <div class="bulat"><span><i
                                                                                class="fa fa-cog icon-spek"
                                                                                aria-hidden="true"></i></span></div>
                                                                    <p class="spek">
                                                                        @if ($item->type == 'mpv')
                                                                            MPV
                                                                        @elseif ($item->type == 'city')
                                                                            City
                                                                        @else
                                                                            Heathback
                                                                        @endif
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="bulat"><span><i
                                                                                class="fa fa-user icon-spek"
                                                                                aria-hidden="true"
                                                                                style="margin-left: 5.5px;"></i></span>
                                                                    </div>
                                                                    <p class="spek">{{ $item->seat }} Orang</p>
                                                                </td>
                                                                @if ($item->audio != 0)
                                                                    <td>
                                                                        <div class="bulat"><span><i
                                                                                    class="fa fa-volume-up icon-spek"
                                                                                    aria-hidden="true"></i></span></div>
                                                                        <p class="spek">Audio</p>
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    <div class="bulat"><span><i
                                                                                class="fa fa-key icon-spek"
                                                                                aria-hidden="true"></i></span></div>
                                                                    <p class="spek">
                                                                        @if ($item->transmisi == 'manual')
                                                                            Manual
                                                                        @else
                                                                            Automatic
                                                                        @endif
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                @if ($item->p3k != 0)
                                                                    <td>
                                                                        <div class="bulat"><span><i
                                                                                    class="fa fa-plus-square icon-spek"
                                                                                    aria-hidden="true"></i></span></div>
                                                                        <p class="spek">P3K</p>
                                                                    </td>
                                                                @endif
                                                                @if ($item->ac != 0)
                                                                    <td>
                                                                        <div class="bulat"><span><i
                                                                                    class="fa fa-snowflake-o icon-spek"
                                                                                    aria-hidden="true"></i></span></div>
                                                                        <p class="spek">Ac</p>
                                                                    </td>
                                                                @endif
                                                                <!-- <td>p3k</td> <td>Ac</td> -->
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    {{-- <div class="tab-pane pane-harga fade tabel-spek"
                                                    id="v-pills-profile-1-{{ $key }}">
                                                    <div class="tabel-harga">
                                                        @if ($item->kunci_12 != 0 || $item->kunci_full != 0)
                                                            <p>Lepas Kunci</p>
                                                            <ul>
                                                                @if ($item->kunci_12 != 0)
                                                                    <li>Rp
                                                                        {{ number_format($item->kunci_12, 0, ',', '.') }} /
                                                                        12 jam</li>
                                                                @endif
                                                                @if ($item->kunci_full != 0)
                                                                    <li>Rp
                                                                        {{ number_format($item->kunci_full, 0, ',', '.') }}
                                                                        /
                                                                        Fullday
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        @endif
                                                        @if ($item->sopir_12 != 0 || $item->sopir_full != 0)
                                                            <p>Dengan Sopir</p>
                                                            <ul>
                                                                @if ($item->sopir_12 != 0)
                                                                    <li>Rp
                                                                        {{ number_format($item->sopir_12, 0, ',', '.') }} /
                                                                        12 jam</li>
                                                                @endif
                                                                @if ($item->sopir_full != 0)
                                                                    <li>Rp
                                                                        {{ number_format($item->sopir_full, 0, ',', '.') }}
                                                                        /
                                                                        Fullday
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        @endif
                                                        @if ($item->all_12 != 0 || $item->all_full != 0)
                                                            <p>Sopir + BBM</p>
                                                            <ul>
                                                                @if ($item->all_12 != 0)
                                                                    <li>Rp {{ number_format($item->all_12, 0, ',', '.') }}
                                                                        /
                                                                        12 jam</li>
                                                                @endif
                                                                @if ($item->all_full != 0)
                                                                    <li>Rp
                                                                        {{ number_format($item->all_full, 0, ',', '.') }} /
                                                                        Fullday</li>
                                                                @endif
                                                            </ul>
                                                        @endif
                                                    </div>
                                                </div> --}}
                                                    <div
                                                        class="tab-pane pane-hargaP-{{ $key }} fade tabel-spek"id="v-pills-profile-{{ $key }}">
                                                        <style>
                                                            .pane-hargaP-{{ $key }} {
                                                                margin-bottom: 85px;
                                                            }

                                                            @media screen and (max-width: 767px) {
                                                                .pane-hargaP-{{ $key }} {
                                                                    margin-bottom: 280px
                                                                }
                                                            }
                                                        </style>
                                                        @if ($item->kunci_12 == 0 && $item->kunci_full == 0)
                                                            <style>
                                                                .pane-hargaP-{{ $key }} {
                                                                    margin-bottom: 0px
                                                                }

                                                                @media screen and (max-width: 767px) {
                                                                    .pane-hargaP-{{ $key }} {
                                                                        margin-bottom: 190px
                                                                    }
                                                                }
                                                            </style>
                                                        @elseif ($item->sopir_12 == 0 && $item->sopir_full == 0)
                                                            <style>
                                                                .pane-hargaP-{{ $key }} {
                                                                    margin-bottom: 0px
                                                                }

                                                                @media screen and (max-width: 767px) {
                                                                    .pane-hargaP-{{ $key }} {
                                                                        margin-bottom: 190px
                                                                    }
                                                                }
                                                            </style>
                                                        @endif
                                                        <div class="tabel-harga">
                                                            @if ($item->kunci_12 != 0 || $item->kunci_full != 0)
                                                                <p>Lepas Kunci</p>
                                                                <ul>
                                                                    @if ($item->kunci_12 != 0)
                                                                        <li>Rp
                                                                            {{ number_format($item->kunci_12, 0, ',', '.') }}
                                                                            /
                                                                            12 jam</li>
                                                                    @endif
                                                                    @if ($item->kunci_full != 0)
                                                                        <li>Rp
                                                                            {{ number_format($item->kunci_full, 0, ',', '.') }}
                                                                            / Fullday
                                                                        </li>
                                                                    @endif
                                                                </ul>
                                                            @endif
                                                            @if ($item->sopir_12 != 0 || $item->sopir_full != 0)
                                                                <p>Dengan Sopir</p>
                                                                <ul>
                                                                    @if ($item->sopir_12 != 0)
                                                                        <li>Rp
                                                                            {{ number_format($item->sopir_12, 0, ',', '.') }}
                                                                            /
                                                                            12 jam</li>
                                                                    @endif
                                                                    @if ($item->sopir_full != 0)
                                                                        <li>Rp
                                                                            {{ number_format($item->sopir_full, 0, ',', '.') }}
                                                                            / Fullday
                                                                        </li>
                                                                    @endif
                                                                </ul>
                                                            @endif
                                                            @if ($item->all_12 != 0 || $item->all_full != 0)
                                                                <p>Sopir + BBM</p>
                                                                <ul>
                                                                    @if ($item->all_12 != 0)
                                                                        <li>Rp
                                                                            {{ number_format($item->all_12, 0, ',', '.') }}
                                                                            / 12 jam</li>
                                                                    @endif
                                                                    @if ($item->all_full != 0)
                                                                        <li>Rp
                                                                            {{ number_format($item->all_full, 0, ',', '.') }}
                                                                            /
                                                                            Fullday</li>
                                                                    @endif
                                                                </ul>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="https://api.whatsapp.com/send?phone=+62{{ Str::substr($profil->kontak, 1, 200) }}&text={{ $profil->text_wa }}"
                                            target="_blank">
                                            <button class="pesan-now">
                                                <p class="pesan">Pesan Sekarang</p>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        @endif

        {{-- <div class="container">
        <div class="row">
            <div class="col-md-12 text-top">
                <p class="title-1 syarat">Butuh Sewa Mobil Jogja Lepas Kunci ? </p>
                <p class="deskripsi-1">Kami Menyediakan Jasa Layanan Rental Mobil di Jogja Lepas Kunci bagi anda yang
                    ingin
                    lebih
                    private ketika Berlibur di Jogja ataupun sekedar urusan bisnis. Harga Penyewaan Mobil Lepas Kunci /
                    Setir
                    sendiri
                    di Jogja bersama kami sangatlah terjangkau Harganya.</p>
                <hr class="garis-1">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3">
                <p class="judul-syarat">Domisili Jogja :</p>
                <div class="desk-syarat">
                    {!! $syarat->domisili !!}
                </div>
            </div>
            <div class="col-md-3 ">
                <p class="judul-syarat">Domisili Luar Jogja :</p>
                <div class="desk-syarat">
                    {!! $syarat->luar_domisili !!}
                </div>
            </div>
            <div class="col-md-3 ">
                <p class="judul-syarat">Mahasiswa :</p>
                <div class="desk-syarat">
                    {!! $syarat->mahasiswa !!}
                </div>
            </div>
            <div class="col-md-3 ">
                <p class="judul-syarat">Turis :</p>
                <div class="desk-syarat">
                    {!! $syarat->foreigner !!}
                </div>
            </div>
        </div>
    </div> --}}

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-top">
                    <p class="title-1 syarat">Testimoni Customer Astha Trans </p>
                    <p class="deskripsi-1">Testimoni para Customer yang sudah menggunakan jasa layanan transportasi dan
                        wisata Astha Trans Melalui
                        Rating Dari Google Reviews</p>
                    <hr class="garis-1">
                </div>
                <div id="priview" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($testimoni as $key => $item)
                            <li data-target="#priview" data-slide-to="{{ $key }}"
                                class="{{ $key == 0 ? 'active' : '' }}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($testimoni as $key => $item)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="testimonial4_slide">
                                    <img class="img-circle img-responsive" src="{{ asset('uploads/' . $item->image) }}"
                                        alt="First slide">
                                    <p>{{ $item->deskripsi }}</p>
                                    <h4>{{ $item->name }}</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <br>
                    <a class="carousel-control-prev" href="#priview" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#priview" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-top">
                <p class="title-1 syarat">Paket Tour Astha </p>
                <p class="deskripsi-1">Astha Trans menawarkan paket wisata untuk Anda yang ingin berlibur bersama kerabat,
                    pasangan, keluarga,
                    rekan kerja atau bisnis. Selain paket wisata yang kami tawarkan, Anda juga bisa membuat list tempat
                    wisata sendiri.
                    Kami siap melayani untuk transportasi dan kebutuhan lainnya untuk wisata Anda.</p>
                <hr class="garis-1">
            </div>
        </div>
        <div class="row">
            @foreach ($wisata as $item)
                <div class="col-md-4 pad-tour" style="margin-top: 40px;">
                    <div class="card paket-tour">
                        <img class="card-img-top img-tour" src="{{ asset('uploads/' . $item->image) }}">
                        <div class=" card-body title-tour">
                            <h5 class="card-title judul-tour">{{ $item->name }}</h5>
                            <p class="card-text  pilihan-tour">{{ $item->tour_count }} Pilihan {{ $item->deskripsi }}</p>
                            <p class="harga-tour">Mulai : Rp. {{ number_format($item->price, 0, ',', '.') }}</p>
                            <a href="{{ route('wisata', $item->slug) }}">
                                <button class="detail-tour">
                                    Lihat Detail
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <br>
            @endforeach
        </div>
        <br>
    </div>
@endsection
@section('include-js')
@endsection
