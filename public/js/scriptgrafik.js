const data = {
	labels : [
		'red',
		'orange',
		'yellow', 
		'green',
		'blue'
	], 
	datasets : [{
		label : 'My First Data Set', 
		data: [900, 300, 500, 200, 700],
		backgroundColor: [
			'red', 'orange', 'yellow', 'green', 'blue'
		],
		hoverOffset: 12
	}]
}


const config = {
	type: 'pie',
	data: data
}

var myChart = new Chart(
		document.getElementById('myChart'),
		config
	);
